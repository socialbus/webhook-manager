npm## Summary

### - 1. [Introduction](#introduction)

### - 2. [Features](#features)

### - 3. [Running a bot in an OSNS](#running)

### - 4. [Socialbus initiating a conversation with a user](#intiate_conversation)

### - 5. [Deploy Socialbus to an OSNS](#deploy)

## 1. Introduction <a name="introduction"></a>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Computer-mediated communication can be defined as any form of human
communication achieved through computer technology. From its beginnings, it has
been shaping the way humans interact with each other, and it has influenced many
areas of society. There exist a plethora of Online Social Network Services (OSNS)
enabling computer-mediated social communication (e.g., Skype, Facebook
Messenger, Telegram, WhatsApp, Twitter, Slack, etc.). Based on personal
preferences, users may prefer a social interaction service rather than another. As a
result, users sharing same interests may not be able to interact since they are using
incompatible technologies. To tackle this interoperability barrier, we propose
Socialbus, a middleware solution targeted to enable the interaction via
heterogeneous social interaction services.

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;The purpose of this documentation is to understand how to setup and use Socialbus. The [figure](#fig1) below shows an example of a user interacting with Socialbus.


![](https://notes.inria.fr/uploads/upload_2e605658074ad2b12d919549fba5a253.png)

*Figure 1: Diagram of a user interacting with Socialbus* <a name="fig1"></a>

A user can communicate to other users through groups that were created and linked
through Socialbus’ website. To have a more in depth understanding of how the
groups work, please refer to the functional website documentation.

## 2. Features <a name="features"></a>

Refer to [this page](link) to see all the features available in Socialbus.

## 3. Running a bot in an OSNS <a name="running"></a>


Socialbus receives messages through separate webhooks for each OSNS. Those webhooks are located in the Servers directory and conventionally named “server[OSNS].js” in the Socialbs repository.

### 3.1 Defining the hostname which receives the webhooks

Socialbus must be deployed in a server and reachable through an https url.
The OSNS needs to know the url to the webhook of the target OSNS.
The webhook URL should be under this form: “https://[socialbus deployment url]/[OSNS’ name]”.
Please review [deployment documentation](tutorial-deploySocialbus.html) to deploy Socialbus.


## 4. Socialbus initiating a conversation with a user <a name="<a name="introduction"></a>"></a>

OSNS usually have security measures that prevent bots from initiating a conversation with any user. 
The user must be authenticated and give their consent to be able to receive messages from the bot. 
Those authentications are made through Sociabus’ website while joining a group.

The procedure is as follows:
1.	Give the OSNS’ webhook URL to the OSNS
2.	Authenticate the user to the OSNS’s application.

## 5. Deploy Socialbus to an OSNS <a name="deploy"></a>

Refer to [this page](link) to see all the tutorials on how to deploy to a specific OSNS.
