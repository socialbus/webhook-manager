## Summary

### - 1. [Introduction](#introduction)

### - 2.  [Deploying to production](#deploy)



## 1. Introduction <a name="introduction"></a>

The purpose of this documentation is to understand how to set up and deploy Socialbus and develop in it.
To understand the general functionality of Socialbus please refer to the [functional documentation](https://socialbus.gitlabpages.inria.fr/webhook-manager/tutorial-functionnalDocumentation.html).  

There are 4 repositories:

- Platform: The main repository. It handles docker-compose and uses other repositories as dependencies
- Webhook-manager: Contains the code and dockerfiles for OSNS' and message sending
- Website: Has the front and the back along with the dockerfiles to build them.

The Platform repositories is the repository which runs the continuous integration.

## 2. Deploying to production <a name="deploy"></a>

To deploy Socialbus, a machien needs to be configured to be accessible through a https url which implies that a trusted SSL certificate is used.
It is recommended to use NGINX to deploy Socialbus, but any other technology can be used.

[Docker](https://docs.docker.com/get-docker/) and [Docker Compose](https://docs.docker.com/compose/install/) need to be downloaded to be able to run the application.

For the continous integration to be able to automatically deploy through gitlab, the machine needs to be registered as a gitlab-runner.

### 2.1 Registering as a gitlab-runner

In the platform repository's settings for CI, there is a Runner tab. This is where the gitlab-runner token can be found.

![](https://notes.inria.fr/uploads/upload_b83328e7aa04843aff0b24e68e81c0b9.png)

*Accessing the registration token for gitlab-runner registration*

The command below registers a machine as a gitlab-runner:

    sudo gitlab-runner register

The url must then be configured:

    https://gitlab.inria.fr

Enter the registration token for the repository:

    exampleToken

Enter a description:

    The production machine that will run Socialbus

Enter the tags:

    main, linux, prod

At least one of the tags must be defined and unique as the continuous integration uses this tag to deploy the application

Provide a runner executor:

    Shell

The gitlab-runner is now registered and can be used by the continious integration. The gitlab-runner appears in the same page the registration token is located.

![](https://notes.inria.fr/uploads/upload_cc039e42286a02b8d0bb9e75a1bbf23e.png)

*Successful registration of a gitlab-runner*

### 2.2 Setting up the environment variables <a name="variables"></a>

To configure Socialbus, many environment variables have to be defined.
For production deployment, these are defined in gitlab environment variables for the platform repository.

The block below shows all the environment variables required for Socialbus:

```
#PORTS
MESSENGERPORT=""
TWITTERPORT=""
SLACKPORT=""
NGINXPORT=""
EXPRESSPORT=""
WEBSITEPORT=""
MONGO_PORT=""

#HOSTNAMES AND PATHS
PROTOCOL=https <-- this can be changed to http for local tests
HOSTNAME="socialbus.paris.inria.fr" <-- has to be changed if hosted in a different domain
MONGO_HOST="mongoDB" <-- name of the mongoDB container
RABBITMQ_HOST="rabbitMQ" <-- name of the rabbitMQ container

#WEBSITE ENV VARS
REACT_APP_EXPRESS_PATH="socialbus.paris.inria.fr/express" <-- Where the website should send GEt and POST requests.
REACT_APP_PROTOCOL=https <-- this can be changed to http for local tests
WEBSITEVOLUME="/usr/share/nginx/react" <-- Where the website is located for deployement

#MESSENGER
MESSENGER_VERIFY_TOKEN="" 
MESSENGER_BOT_ID=""
MESSENGER_BOT_TOKEN=""

#TWITTER
TWITTER_CONSUMER_KEY=""
TWITTER_CONSUMER_SECRET=""
TWITTER_TOKEN=""
TWITTER_TOKEN_SECRET=""
TWITTER_BOT_ID=""
TWITTER_APP_NAME=""

#SLACK
SLACK_CLIENT_ID=""
SLACK_CLIENT_SECRET=""
SLACK_BOT_TOKEN=""
SLACK_VERIFY_TOKEN=""
```

### 2.3 Launching the deployment pipeline

The continuous integration can be found in the ".gitlab-ci.yml" file in the [Platform repository](https://gitlab.inria.fr/socialbus/Platform/-/blob/master/.gitlab-ci.yml).

In its process, each docker image needed to run Socialbus is built and pushed to the docker image registry contained in the repository.

Deployment process are only started if a commit was made to the master branch and a tag was created starting with "v*".
For example, if a new version of Socialbus was pushed, the developer can create the tag "v1.0.10" which will trigger the automatic deployment.

The tag version must be provided in the variables of the ".gitlab-ci.yml" file. This is the version that is deployed, this provides a possibility to easily change between versions of the application.

![](https://notes.inria.fr/uploads/upload_91bc204f3c782aad772bbeb66e66bfb3.png)

*The version v1.0.6 will be deployed when a tag is created*

### 2.4 Launching tests

The tests are launched when code is committed on the master branch of the subdirectories webhook-manager and website.

### 2.5 Docker container registry

When a deployment is started, every image is built and pushed to the docker registry. They are tagged with the commit code and the version tagged. These can be accessed in the "Packages & Registries" tab of the gitlab repository.

![](https://notes.inria.fr/uploads/upload_58f6d46ec4eecd3f348e0b76b6fbefbc.png)

*Example of a docker container registry*

Once all the images are built and pushed, they can be accessed from the test and production machines to be launched.
