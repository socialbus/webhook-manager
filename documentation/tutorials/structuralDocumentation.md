## Summary

### - 1. [Introduction](#introduction)

### - 2. [Structure Diagram](#structure_diagram)

### - 3. [Structure Flow](#structure_flow)

### - 4. [Technology choices](#technology_choices)

## 1. Introduction <a name="introduction"></a>

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To understand the general functionality of Socialbus please refer to the functional documentation. The purpose of this documentation is to understand the structure of Socialbus works in a more technical view and the possible future improvements. It will explain how messages are interpreted and sent to other users, what technologies were used for Socialbus and why. 

## 2. Structure Diagram <a name="structure_diagram"></a>

The diagram below represents the structure of the Webhook Manager of Socialbus.

![](https://notes.inria.fr/uploads/upload_faaf7c043dc3dadfbc6d8d4278de5a7f.png)

*Figure 1: Diagram of Webhook Manager's structure* <a name="fig1"></a>

## 3. Structure Flow <a name="structure_flow"></a>

Each entity from [figure1](#fig1) are described more in detail below. They can also be found in the developper documentation.

### 3.1.a Servers
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Each OSNS uses a webhook which sends data to Socialbus. Those webhooks are located in the Servers directory and conventionally named “server[OSNS].js” in the Socialbus repository.

For example, Twitter’s server is called “serverTwitter.js”. 
The data received is a JSON which is formatted differently in every OSNS. 
The servers translate the format of those JSONs received to a uniform JSON that can be interpreted by Socialbus, no matter its origin.  

### 3.1.b Uniform JSON format <a name="json_format"></a>

```json=
connectors:
{
    //any information related to the sender of the message such as user id, channels, conversation tokens...
}
text: "the message intended to be sent back to the user(s).",
origin: "the OSNS the user's message originates from. If the targetGroup field is undefnied, the message is sent to its origin.",
targetGroup: "If this message is to be sent to a group, its id will be stored here. otherwhise, this field is undefined",
buttonTemplate: "If this message requires a button template, its type is defined here."
buttonID: "The button ID pressed by the user if any"

```

*Figure 2: Socialbus' uniform JSON format*

### 3.2 [dataTranslator.js](link)

Based on the state of the user and their command, dataTranslator will determine what the bot should reply and affect the user's state if needed.

### 3.3 [send.js](link)

Oversees sending the translated JSON to the targeted queue.

### 3.4 [receiveQueues.js](link)

All the queues’ incoming messages are received and are then sent to postToService.js.


### 3.5	[buildTemplates.js](link)

Before the JSON is sent if a button template is requested, Buildtemplates.js transmits the JSON for the requested template for the target OSNS. 

### 3.6	[mongoose.js](link)

Communicates with the database, validates the data, and executes queries.

## 4. Technology choices <a name="technology_choices"></a>

### 4.1	NodeJS

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;NodeJS is a very popular technology for backend server side javascript.
It is a good choice for Socialbus since it is capable of handling event-driven structures asynchronously. It is scalable and easy to use. 

### 4.2	 ExpressJS

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ExpressJS is one of the most popular, easy to use while still robust API middleware. It is used in Socialbus for example to receive messages from OSNS’, or for communication between the database and the website.

### 4.3	 MongoDB

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MongoDB is a database program that does not need a predetermined data structure. It takes data in the form of JSON which suits the rest of the data structure from Socialbus and makes it easy and practical to implement.

### 4.4	React

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;React is a frontend JavaScript library for Single Page Application (SPA). 
It allows the use of 3rd party libraries which makes it flexible but is also simpler and more modular than its competitors. It is also more lightweight and easier to use which is faster to put into place.  

### 4.5	Nginx

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nginx is a web server software. It manages the redirection into its various webhooks, application, and website. It also takes care of the SSL certificate so that it can be accessed publicly with an https protocol. The architecture of Nginx being asynchronous, and event based, allows for better scalability and accessibility than its competitors. 
The event-based nature with separate processes of Socialbus fits well with Nginx’s workflow. 

### 4.6	Docker

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Docker is a software for OS level virtualization. It creates isolated containers with their own dependencies and environment. The microservice structure enabled from docker makes a software more manageable. For example, you can control dependencies, run time issues independently. 
It is very useful for deployment as the host machine does not need anything else to run a container. 
It will also be useful in the future for unit testing since the environment of Socialbus will be easily reproducible. 

### 4.7	Docker-compose

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Docker-compose is a docker orchestrator which makes configuring the docker containers, managing communication between them and defining their environment much easier. In Socialbus’ context, it ensures the dependency between each container is met, that the environment variables are applied and configures the local network between them.

### 4.8	Gitlab-ci
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gitlab-ci is an easy to use, no installation required continuous integration tool since Socialbus is already a repository on gitlab. The runners required to run them are available from Inria with ci.inria.fr. 
