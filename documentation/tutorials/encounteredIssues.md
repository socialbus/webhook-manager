This page describes the various issues encountered during the development and provides solutions for them. The aim is to prevent future developers from encountering similar issues with no anwser.

## Summary

### - 1. [Communication between docker containers](#implementing)

### - 2. [Deploying a react application](#deploy)

### - 3. [Volume permission issues for react build](#volume)

### - 4. [Container dependency issues](#container)

### - 5. [Circular dependency issue from two instances of docker-compose](#circular)

### - 6. [Space management errors](#space)

### - 7. [SSL certificate migration issues](#ssl)

### - 8. [SSH connection issues](#ssh)

### - 9. [Nginx subpath issue with redirections to express](#nginx)

### - 10. [CORS interaction issue with NGINX + Docker](#cors)

### - 11. [Partition space issues](#partition)

### - 12. [Unable to locate package](#package)


## 1. Communication between docker containers <a name="communication"></a>

In Socialbus, some docker containers need to communicate between each other outside of the queuing system from RabbitMQ. For instance, the receiver container needs to communicate with the database.
To do that, it is necessary to create a local network containing those containers. 

### Creating a network

The command below creates a docker network:

    Docker networks create [network name]
    
### Plugging the network to the containers

In the docker-compose file, it is possible to configure a network for containers. 

### Define the use of the created network


```yaml=
networks:
  [network name]:
    driver: bridge
```

### Attribute the network to a container

```yaml=
  receiver:
    container_name: "receiver"
    build:
      context: ./webhook-manager
      dockerfile: ./docker/Dockerfile.Receiver
    env_file:
      - .env
    networks:
      - [network name]
```

### Communication

The next step is communicating with another container from within one. The containers that are being communicated with have a port exposed and linked to the host machine with docker-compose. However, it is not possible to access a port just with:

    localhost:[port]
It is instead accessed with the hostname:

    [dockerContainer]:[port] 

## 2. Deploying a react application <a name="deploy"></a>

When using the "npm start" command, node launches the react application, hosted on a local port. This is not meant to be used for production purposes but only for development environment. 

React generates static files. This means the application doesn't need to be ran. Instead, a build of the react application is stored in files needs. They can then be deployed with a reverse-proxy technology such as Nginx.

### Sharing a volume through docker-compose

The "volumes" key defines volumes that are shared from the host machine to the docker-compose. The volume where the container should be stored should be defined as an environement variable.

```yaml=
  react:
    container_name: "website"
    build:
      context: ./website
      dockerfile: ./docker/Dockerfile.Website
    env_file:
      - .env
    volumes:
      - $WEBSITEVOLUME:/build
```

### Making a production build of a react application

With the "command" key it is possible to executes commands after the container is built and running. It will automatically close the container upon executing the commands.

In this case, once the react container is done building, we want the react app to build at the dedicated volume.

```yaml=
  react:
    container_name: "website"
    build:
      context: ./website
      dockerfile: ./docker/Dockerfile.Website
    env_file:
      - .env
    volumes:
      - $WEBSITEVOLUME:/build
    command: bash -c "npm run build"
```

## Redirecting the static files to a location in Nginx

In Socialbus' case, we want the users to access the website with the default location of the domain. We give the default location of the root folder where the build was directed on the host machine as shown below.

```yaml=
location / { #Default location
    include /etc/nginx/mime.types;
    root   /usr/share/nginx/react; #Directory to redirect to
    index  index.html index.htm;
    try_files $uri $uri/ /index.html;
}
```

## 3. Volume permission issues for react build <a name="volume"></a>

As a security measure, docker containers have the same permissions as the user launching the docker-compose. 
If the user does not have the writing permissions to write in the given directory for the build, docker cannot write files either. 

This issue took me a long time to figure out as it happened after migrating virtual machines for Socialbus. It can be misleading because no errors are given, the files are just not getting written. 

### Giving permissions to the directory

On the shared volume’s folder, use the command:

    chmod 700 [foldername]

the user using this command now has the permissions to read, write and execute on that folder, which means docker does as well.

### 4. Container dependency issues <a name="container"></a>

Some containers' tasks rely on other containers. They need to be able to wait for each other before running their own functions. 

### "Depends on" command

In a docker-compose, the “depends_on” key makes it so that the container is not ran until all the other containers it depends on are running. 

For example, the receiver container relies on RabbitMQ to execute its tasks.

This is where the "depends_on" key is used.

```yaml=
  receiver:
    container_name: "receiver"
    build:
      context: ./webhook-manager
      dockerfile: ./docker/Dockerfile.Receiver
    env_file:
      - .env
    depends_on:
      - rabbitmq
```

In this case, the receiver container will not start until the RabbitMQ is started. Docker-compose automatically figures out the ordering in which the containers are ran. 

### Waiting for a dependency container's task to be ready

Some containers do commands after being started. Containers that depend on them need to be wait for those containers to be ready before starting their own tasks. 
The solution is to use a bash script called “wait-for-it.sh” which waits on the availability of a host and port. Once that host and port are available, it is possible to run any command such as launching a part of a software in a container. 

In the case of the receiver container, we need to wait for the rabbitMQ container not only to have started, but launched rabbitMQ and exposed the port.

```yaml=
  receiver:
    container_name: "receiver"
    build:
      context: ./webhook-manager
      dockerfile: ./docker/Dockerfile.Receiver
    env_file:
      - .env
    depends_on:
      - rabbitmq
    command: ["./wait-for-it.sh", "rabbitmq:5672", "-t", "120", "--", "bash", "launchReceive.sh"]
```

Let's break down this command: 

    ./wait-for-it.sh         Launch the wait-for-it bash script
    rabbitmq:5672            Host and port to await.
    -t 120                   The amount of time to wait before timing out
    bash launchReceive.sh    The task to apply upon connecting to rabbitMQ.
    
The containers are now launched in the right order and waiting for the containers to be available.

## 5. Circular dependency issue from two instances of docker-compose <a name="circular"></a>

Initially there were two instances of docker-compose:
- One for Socialbus 
- One for the Website 

They are separated in different repositories. 
This caused a circular dependency from both instances of the docker-compose as shown in the diagram below:

![](https://notes.inria.fr/uploads/upload_c4d80aad2614ee42c20d2d96bb5df2bc.png)

*Diagram of a circular dependency*

In this case, Express needs Database to be launched on start and Reverse-proxy needs Express to be launched on start. 
To fix this issue, both docker-compose instances were combined into one in the sub-directory of both folders. This led to a lot of refactoring and changes in the logic but is more logical structure wise. The structure is as follows:

![](https://notes.inria.fr/uploads/upload_c5100ed4d29957a527c1697de8ac9bec.png)

*Docker-compose structure*

The parent directory "Platform" gets the sub directories from git submodules. It then setups the environement with the .env file and uses the docker-compose file to launch various containers of the application.

### Refactoring docker-compose as a single instance

Thanks to this structure, it is easy to refactor the docker-compose into a single instance. It is possible to give a specific context with the "context" key for each container’s build process.  

```yaml=
 express:
    container_name: "express"
    hostname: "express"
    build:
      context: ./website #Website's directory
      dockerfile: ./docker/Dockerfile.Express
    env_file:
      - .env
  messenger:
    container_name: "messenger"
    build:
      context: ./webhook-manager #Webhook-Manager's directory
      dockerfile: ./docker/Dockerfile.Messenger
    env_file:
      - .env
```

## 6. Space management errors <a name="space"></a>

The previous virtual machine that was hosting Socialbus had very little space allocated which caused space issues. 
Launching the docker-composes sometimes will trigger errors saying, “No space left on device”.  There are multiple things that can be done to free up space. 


### CleanSpace .sh
A script called cleanSpace. sh is located at the root directory "Platform". It stops all the containers, removes all the docker images, all the docker containers and deletes all RabbitMQ cache data.

Run the script as root for it to delete the RabbitMQ data.

### Cleaning space manually 

First, we can remove all the running containers of Socialbus by going in the repositories and using the command:

    
    docker-compose down
Then, we can make sure to remove all dangling containers with the command:

    docker rm $(docker ps -a -q) 

If the command gives an error saying missing arguments, it means no containers are running.
After that, we can remove all the images by using the command:

    
    docker rmi $(docker images -a -q).

RabbitMQ's cache data also takes a lot of space. Socialbus does not require to use all the saved data and logs as channels are just recreated automatically if they do not exist. 

Those can be removed by deleting the folder “webhook-maanger/docker/resources/rabbitmq/lib/mnesia” with the command:

    sudo rm -R path/to/folder/mnesia
The folder will automatically be recreated when the container is launched.


## 7. SSL certificate migration issues <a name="ssl"></a>

SSL certificates are used for encrypted connections, allowing https protocols. It is necessary to use one to be able to access Socialbus publicly. Upon the migration of Socialbus’ host machine, the ssl certificate needed to be modified to redirect to the new host ip. 
A redirection of the hostname was made so that the previous hostname redirects to the new one.


## 8. SSH connection issues <a name="ssh"></a>

To be able to connect to the virtual machine containing Socialbus, it is necessary to pass security measures.

## Connect to the Inria VPN

It is a security step that lets only Inria users to connect to the VPN.
Connect to a VPN by using a software made for it. The most common one is Cisco AnyConnect. 
Connect to “vpn.inria.fr” and login with an Inria account. 

![](https://notes.inria.fr/uploads/upload_d5d013a3b38f53ca6c2e9b5f9841c26e.png)

*Connection to Inria's VPN*

### Generate a ssh key

To prevent anyone from connecting to the virtual machine, you must generate a ssh key to identify yourself.  
From the machine that will connect to the virtual machine, use the command below to generate a key:

    ssh-keygen -r -rsa -b 4096
    
Follow the instructions.
This will generate a public and a private key.
They are stored in 2 files:
-	[name of key] file contains the private key.
-	[name of key].pub file contains the public key.

### Giving the public key to Inria’s bastion host

This cannot be done manually, you need to create a ticket through inria’s helpdesk so a technician puts it in place.
When creating the ticket, a category must be chosen. 
The one we need is “SSH access on Secure server”.

![](https://notes.inria.fr/uploads/upload_55cfa0f5d89c955250ef532ee6c835de.png)

*Ticket creation process on Inria's HelpDesk*

## 9. Nginx subpath issue with redirections to express <a name="nginx"></a>

Nginx worked fine with redirections to a direct path but had a lot of issues with subpaths.

A direct path implies redirecting to an exact location, for example the path “https://www.socialbus.paris.inria.fr/twitter” will always redirect to the exact page we want.

```yaml=

      location /twitter {
          resolver 127.0.0.11 valid=30s;
          set $twitter_up twitter:5001;
          proxy_pass          http://$twitter_up/webhook?$args;
          proxy_set_header    X-Forwarded-For $remote_addr;
      }

```

In this case, a subpath such as “https://socialbus.paris.inria.fr/twitter/subPath” will never be used.
However in Express’ case, we want to be able to access various subpaths dynamically using it as the example below: 

```jsx=
// the url https://socialbus.paris.inria/express/create?aGroupeName redirects to:
app.get('/create/:groupename', function(req, res, next) {
    console.log("request param is ", req.params);
    res.send('hello world');
});
```

### Nginx implementation for dynamic subpaths

To be able to redirect to dynamic subpaths properly, requirements need to be met:
- Express needs to be started before Nginx is ran
- Nginx location directive needs to specify that it takes relative paths
There are several directive modifiers that can be used:
- /: default location
- /specificPath: nginx tries to match the most specific prefix found
- /specificPath/: nginx matches any request that has this path but tries to find a more specific location with a more precise subpath. 

The same rules apply to the proxy_pass path given to nginx. 

```yaml= 

      location /express/ { #The last slash is what matters
          resolver 127.0.0.11 valid=30s;
          set $express_up express:3000;
          proxy_pass          http://express:3000/; #The last slash is what matters
          proxy_set_header    X-Forwarded-For $remote_addr;
      }
```

The configuration above will send requests that have express and subpaths to express and will only receive the subpath in express.
While this solution is rather simple, understanding how express and Nginx interact with one another took a long time, as well as understanding the source of multiple interpretations of the path. 


## 10. CORS interaction issue with NGINX + Docker <a name="cors"></a>

Cross-origin resource sharing (CORS) is a security mechanism which prevents https requests from origins that are not allowed by the application. 

Trying to communicate with the backend of Socialbus from the website, CORS methods were preventing requests.

It seemed likethe issue was that the domain for the website wasn’t accepted by the application, but the issue was attempting to do https requests in the server within the server. Websites are ran in the user’s browser and not on the server side which means requests to Socialbus have to be made through nginx and not locally. 


## 11. Partition space issues <a name="partition"></a>

It is important to define which volume in the machine to use in order to have enough space for various tasks such as building docker images and where the continous integration should build projects and store files.

The various volumes and their sizes can be seen with the command below:

    df -h

### Docker path

Generated files for docker will be at the path where the docker-compose is located. Volumes defined for the containers should be spacious ones.

### Gitlab-runner build path

The build path for Gitlab-runner can be defined in the configuration file for each runner. It is located in the file /etc/gitlab-runner/config.toml.

In that file the "builds_dir" key defines where the build should be located

```bash=
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "main machine running the production application"
  url = "URL"
  token = "TOKEN"
  executor = "shell"
  builds_dir = "/builds/socialbus/webhook-manager/" # defines where the build should be done
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]

[[runners]]
  name = "main machine running the production application"
  url = "URL"
  token = "TOKEN"
  executor = "shell"
  builds_dir = "/builds/socialbus/website/" # defines where the build should be done
  [runners.custom_build_dir]
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
```

## 12. Unable to locate package <a name="package"></a>

When a linux machine is unable to locate a package, it unables the use of commands such as "apt install" and "apt update". 
The image below represents this issue.

![](https://notes.inria.fr/uploads/upload_5f0d9df6c7205fc9363f03991c38bee0.png)

*Unable to locate package issue occuring*

In the case of Socialbus, this has happened upon installing gitlab-runner for the countinous integration. This is caused by an issue with ubuntu 20 within the source list files.

Source list files define which packages linux should try to update and retrieve. 

To fix this issue, removing the source list file of the specific package is neccessary.
