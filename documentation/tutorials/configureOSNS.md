## Deploy Socialbus to Messenger

Facebook developer: https://developers.facebook.com
In Facebook’s developer website log in with an administrator account of Socialbus and go to Socialbus’ app page. Click on webhooks and “edit subscription”.
Alternatively, this link directs to it if logged in as an admin account of Socialbus: https://developers.facebook.com/apps/2098446187090336/webhooks/ 
Enter the URL to the webhook.
In the verification token field enter the generated token.

![](https://notes.inria.fr/uploads/upload_9195c9706858ebe22afa0db61a157cd9.png)

**Example of a subscription**


## Deploy Socialbus to Slack 

Slack API website: https://api.slack.com/ 
In Slack’s API website log in with an administrator account of Socialbus and go to Socialbus’ app page. Click on “event subscriptions” under “features”.
Alternatively, this link directs to it if logged in as an admin account of Socialbus: https://api.slack.com/apps/ARXNUPW8G/event-subscriptions
Enter the URL to the webhook 

![](https://notes.inria.fr/uploads/upload_e75ff631c75a3227af1c82012e781059.png)

*Slack url subscription*

Click on “Interactivity & Shortcuts”
Enter the webhook’s URL

![](https://notes.inria.fr/uploads/upload_00fffbad14953333f3c4bf73b197ae3d.png)

*Slack button url subscription*

## Deploy Socialbus to Twitter

Twitter has a way of functioning that is very different than the other OSNS. 
You must create a webhook manually and give it the link to the webhook rather than feeding it into an interface.
Twitter has made a repository that allows to easily create webhooks through the command line: https://github.com/twitterdev/account-activity-dashboard
Clone the repository and create a .env file at the root of the project and feed it the correct Twitter information such as:

	TWITTER_CONSUMER_KEY= your twitter consumer key
	TWITTER_CONSUMER_SECRET= your twitter consumer secret key 
	TWITTER_ACCESS_TOKEN= your twitter access token
	TWITTER_ACCESS_TOKEN_SECRET= your secret twitter access token 

That information can be accessed through the admin page of the Twitter dev page: https://developer.twitter.com/en
Click on developer portal  Socialbus  apps  keys & tokens. 
Direct access link: https://developer.twitter.com/en/apps/18120626

With a terminal open at the root of the project, you may then use commands to create and delete webhooks, or authenticate users. 
Delete all previous webhooks: 

	node example_scripts/webhook_management/delete-webhook-config.js -e Socialbus

Create a new webhook:

	node example_scripts/webhook_management/create-webhook-config.js -e Socialbus -u <ngrok url>/webhook

Authenticate a user:
Either use Socialbus’ website or use the command:

	node example_scripts/subscription_management/add-subscription-other-user.js -e Socialbus
