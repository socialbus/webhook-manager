This page lists all available commands of socialbus with a detailed description.

## Summary

### - [Help](#help)

### - [Unrecognized commands](#unrecognized_commands)

### - [ConnectToGroup](#connect_to_group)

### - [OpenSession](#open_session)

### - [QuitSession](#quit_session)

### - [GetConnectedMembers](#get_connected_members)

### - [GetAllMembers](#get_all_members)

### - [GetConnectedGroups](#get_connected_groups)

## Commands 

### help <a name="help"></a>

Sends a message to the user that lists every command with a short summary of their effect.

![](https://notes.inria.fr/uploads/upload_33583c093eab745ad2eb49be9f24c376.png)

*Use of the help command*

### unrecognized command <a name="unrecognized_commands"></a>

If the user types a command that does not exist, they will be suggested to type the "help" command to learn about available commands.

![](https://notes.inria.fr/uploads/upload_14164cf6b56726e3597b1e4b35bd25c6.png)

*Example of an unrecognized command*


### connectToGroup <a name="connect_to_group"></a>

Allows the user to receive messages from the group they connect to in real time. 
Upon connection, the user receives the last 30 messages of the conversation to have context of the current state of the discussion.

If the command is not followed by a group name they have joined, they will be prompted with a list of their groups to connect to. Otherwhise, they will directly connect to the joined group. 

![](https://notes.inria.fr/uploads/upload_9894700be2c0de20e0a996a5fbad7ebe.png)

*Example of a user connecting to a group*

### openSession <a name="open_session"></a>

When the user is connected to at least one group, the user can open a session to it. While the session is opened, every message sent to the bot afterwards are directly sent to all the members of the group.

Only one session can be opened at a time.

If the command is not followed by a group name they have connected to, they will be prompted with a list of the groups they are connected to. Otherwhise, they will directly open a session to the joined group. 

Lorsque l'utilisateur est connecté à au moins un groupe, l'utilisateur peut ouvrir une session. Tant que la session est ouverte, les prochains messages que l'utilisateur envoie au bot sur le réseau social est envoyé à tous les autres membres du groupe.
Qu'une seul session peut être ouverte à la fois


![](https://notes.inria.fr/uploads/upload_16db0beed6e08913d56f6f78a15c513c.png)

*Example of a user opening a session to reply to a message*

### quitSession <a name="quit_session"></a>

If the user has an opened session, quits the session. Otherwhise, the bot informs them that they don't have a session opened.

### getConnectedMembers <a name="get_connected_members"></a>

Lists the users currently connected to a specific group.

### getAllMembers <a name="get_all_members"></a>

Lists all the users connected to the group, currently receiving messages in real time. 
Liste tous les utilisateurs du groupe.

### getConnectedGroups<a name="get_connected_groups"></a>

Lists the groups the user is currently connected to.