Socialbus has been dockerized to be able to deploy Socialbus on any machine. It also makes various components of Socialbus independent from one another.

## Summary

### - 1. [Containers](#containers)

### - 2. [Containers diagram](#diagram)

### - 3. [Container flow](#flow)

### - 4. [Redirecting to  the application](#redirect)


### 1. Containers <a name="containers"></a>

Each autonomous part of Socialbus has been divided into smaller docker containers:

-	One for each OSNS (currently Messenger, Twitter, Slack)
-	The database
-	The reverse proxy
-	RabbitMQ
-	Message receiver
-	building the frontend of the website
-	building the documentation of Socialbus
-	The backend of the website
-	A docker-compose allows you to orchestrate and launch everything smoothly.

### 2. Containers diagram <a name="diagram"></a>

The diagram below represents the docker containers used in Socialbus.

![](https://notes.inria.fr/uploads/upload_e0df465bb32778140f209b7233f944ee.png)
*Figure 1: Diagram of Socialbus' docker containers* <a name="fig1"></a>

### 3. Container flow <a name="flow"></a>

#### Docker-compose

Orchestrates the launching of Socialbus. It manages the launch of docker containers, the order in which they are launched, the environment variables, and the ports allocated to each container.

#### Webhook Manager

##### OSNS' webhooks

Each OSNS has its own instance of a webhook container. To have more information on what they do, refer to the [structural documentation](https://socialbus.gitlabpages.inria.fr/webhook-manager/tutorial-structuralDocumentation.html).

##### Receiver

The receiver container listens to RabbitMQ queues receiving data an OSNS’ webhook. It then prepares the message for the targeted OSNS' and sends the messages in them.

##### Documentation

This container makes a build of the documentation for the webhook-manager and stores it in a shared volume of the main machine. That shared volume is what the reverse proxy redirects to. The container then stops itself.
Statically generates the documentation for the webhook manager.

#### General

Containers categorized in general are used by other containers to function or to communicate between each other.

##### Database

Container that stores all the data from Socialbus and its website with a mongoDB database. For instance, it stores the user groups created by the website, the users that joined them, etc.

##### RabbitMQ

Implements a queuing system for OSNS’ to communicate data to the receiver container.

##### Reverse-proxy

NGINX Web server that allows users to access the website, documentation and OSNS’ to communicate with Socialbus.

#### Website

##### React

This container makes a production build of the website and stores it in a shared volume of the main machine. That shared volume is what the reverse proxy redirects to. The container then stops itself.

##### Express

Receives Get and Post requests from the website and communicates with the database to do various tasks with users and groups such as creating groups, joining a group, etc.

##### Documentation

This container makes a build of the documentation for the webhook-manager and stores it in a shared volume of the main machine. That shared volume is what the reverse proxy redirects to. The container then stops itself.
Statically generates the documentation for the webhook manager.

### 4. Redirecting to  the application <a name="redirect"></a>

To function, Socialbus needs to be accessible through a https url which implies that a trusted SSL certificate is used.
It is recommended to use NGINX to deploy Socialbus, but any other technology can be used.

#### Website paths

- Default path: redirects to the website

#### OSNS paths

- Each OSNS must have its own path which redirects to the docker container of the targeted OSNS at the /webhook location of the given port.

The format of a redirection is as below:
http://[containerName]:[containerPort]/webhook.