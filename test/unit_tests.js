const chai = require('chai');
const chaiHttp = require("chai-http");
const express = require("../src/servers/serverTwitter");
const databaseManager = require('../src/mongoose');
const mongoose = require('mongoose');

chai.should();
chai.use(chaiHttp);

describe("test twitter webhook", function () {
	it("Should respond 400 to bogus challenge", (done) =>
	{
		chai.request(express)
			.get("/webhook")
			.end((err,response) => {
				response.should.have.status('400');
				done();
			});
	});

	it("Should respond 200 to correct challenge", (done)=>
	{
		chai.request(express)
			.get("/webhook")
			.query({crc_token:"token"})
			.end((err,response) => {
				response.should.have.status('200');
				done();
			});
	})

})

describe("test database communication and queries", function(){

	let user;
	beforeEach(async function() {
		user = await databaseManager.createBlankUser();
	});

	afterEach(async function(){
		await databaseManager.deleteUserByUserID(user._id);
	})

	it("Should access created user by ID", async function()
	{
		const foundUser = await databaseManager.findUserByUserID(user._id);
		const foundUserID = foundUser._id.toString();
		foundUserID.should.equal(user._id.toString());
	})

	it("Should not find user by bogus ID", async function(){
		const bogusID = new mongoose.Types.ObjectId();
		const foundUser = await databaseManager.findUserByUserID(bogusID);
		bogusID.should.not.equal(user._id);
		(foundUser === null).should.be.true;
	})
})