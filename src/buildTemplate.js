/**
 * @module buildTemplate
 * @category Core
 * @description buildTemplate provides button templates for specific OSNS' in Socialbus.
 */

/**
 *
 * Provides the button template type in the JSON format of the defined media with the options based on the parameters.
 * All these informations are in the template parameter.
 * @param {{type : string, command: string, options:JSON}} template - Button template type, the command callback to send, the options the user can press.
 * @param {string} media - The OSNS to provide the button template to.
 * @param {string} userMediaID - The user's media ID.
 * @returns {JSON} The generated button template.
 */
function buildButtonTemplate(template, media, userMediaID) {

    if(template.type === "talkToGroup")
    {
        console.log("Generating talkToGroup");
        if(userMediaID !== undefined)
            return getTemplateTalkToGroup(media,userMediaID, template);
        console.log("userID is not defined.");
    }
}

/**
 *
 * Provides the button template to select a group. The JSON format is defined by media with the options based on the parameters.
 * @param {string} media - The OSNS to provide the button template to.
 * @param {string} userID - The user's media ID.
 * @param {{type : string, command: string, options:JSON}} template - Button template type, the command callback to send, the options the user can press.
 * @returns {JSON}
 */
function getTemplateTalkToGroup(media, userID, template)
{
    const title = "Select a group";

    let json;
    let optionJson;

    if(media === "Messenger")
    {
        json = {
            "type": "template",
            "payload": {
                "template_type": "generic",
                "elements": [{
                    "title": title,
                    "subtitle": "Tap a button to answer.",
                    "buttons": []
                }]
            }
        }

        for(let i = 0; i < template.options.length; i++)
        {
            optionJson = {
                "type": "postback",
                "title": template.options[i].optionName,
                "payload": template.command + "_" + template.options[i].optionValue
            }
            json.payload.elements[0].buttons[i] = optionJson;
            optionJson = {};
        }
        return json;
    }
    else if(media === "Twitter")
    {
        json = {
            text:title,
            quick_reply:
                {
                    "type":"options",
                    "options":[]
                }
        };

        for(let i = 0; i < template.options.length; i++)
        {
            optionJson = {
                "label": template.options[i].optionName,
                "description": "Group " + i,
                "metadata": template.command + "_" + template.options[i].optionValue
            }
            json.quick_reply.options[i] = optionJson;
            optionJson = {};
        }
        return json;
    }
    else if(media === "Slack")
    {
        json = {
            "type": "section",
            "block_id": "section678",
            "text": {
                "type": "mrkdwn",
                "text": title
            },
            "accessory": {
                "action_id": "text1234",
                "type": "static_select",
                "placeholder": {
                    "type": "plain_text",
                    "text": "Select an item"
                },
                "options":[]
            }
        };

        for(let i = 0; i < template.options.length; i++)
        {
            optionJson = {
                "text":{
                    "type": "plain_text",
                    "text": template.options[i].optionName
                },
                "value": template.command + "_" + template.options[i].optionValue
            }
            json.accessory.options[i] = optionJson;
            optionJson = {};
        }
        return ([json]);
    }
}

module.exports  = {buildButtonTemplate};
