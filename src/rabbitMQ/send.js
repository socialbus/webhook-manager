const amqp = require('amqplib/callback_api');

function onConnect(err, connection, message, queue)
{
    if (err)
        throw err;
    connection.createChannel(function(err2, channel)
    {
        if (err2)
            throw err;
        channel.assertQueue(queue, {
            durable: true
        });

        channel.sendToQueue(queue, Buffer.from(message), {
            persistent : true
        });
        console.log("[x] Sent message: " + message);
    });
}

function sendMessage(message, queue)
{

        amqp.connect('amqp://' + (process.env.RABBITMQ_HOST || "127.0.0.1"), (err, connection) => onConnect(err,connection, message, queue));
}

module.exports=  {sendMessage};
