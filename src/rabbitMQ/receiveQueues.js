#!/usr/bin/env node
const amqp = require('amqplib/callback_api');
const mqt = require('./send.js');
const postManager = require('../postToService.js');
const queues = ['Slack', 'Messenger', 'Twitter'];

function onConnect(err, connection)
{
    if(err)
        throw err;


    for(let i = 0; i < queues.length; i++)
    {
        console.log("consuming");
        consumeQueue(queues[i], connection);
    }

}

function consumeQueue(queue, connection)
{
    connection.createChannel(function(err1, channel)
    {
        if(err1)
            throw err1;
        channel.assertQueue(queue, {
            durable : true
        });

        console.log(" [*] waiting for messages in %s.", queue);

        channel.consume(queue, function(message)
        {
            console.log(" [x] received \'%s\'", message.content.toString(), " in queue " + queue);

            let messageString = message.content.toString();
            let messageJson = JSON.parse(messageString);

            let actions = messageJson.databaseAction;

            if (actions !== undefined)
            {
                console.log("query string is : " + messageString);
                mqt.sendMessage(messageString,"Database");
            }
            else
            {
                postManager.postToPlatform(messageJson);
            }

            //keep line below at the end of task
            channel.ack(message);

        }, {
            noAck: false
        });
    });
}

amqp.connect('amqp://rabbitMQ', (err, connection) => onConnect(err, connection));
