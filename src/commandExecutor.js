/**
 * @module commandExecutor
 * @category Core
 * @description Executes commands interpreted from dataTranslator.
 * Prepares the data to send the user and modifies state of users if needed.
 * Verify functions verify if it is possible to know which group or member is targeted. If they aren't, we send a button template to choose an option from instead.
 * */

const databaseManager = require("./mongoose");
const commandHelper = require("./commandHelper");
const postHandler = require('./postToService');

/**
 *
 * Lists all the users in target group. The connected and disconnected users are seperated.
 * @param data - The current data to send.
 * @param targetGroupID - The database ID of the group to list the members from.
 * @returns {Promise<JSON>} - The modified data to send to the user.
 */
async function ExecuteGetAllMembers(data, targetGroupID)
{
    const targetGroup = await databaseManager.findGroupByGroupID(targetGroupID);

    let outputMessage = "";
    outputMessage = "There are currently " + targetGroup.users.length + " users that joined the group " + targetGroup.groupName + ". Those users are: ";
    const groupUsers = targetGroup.users;
    let connectedUsers = "";
    let disconnectedUsers = "";
    for(let i = 0; i < groupUsers.length; ++i)
    {
        if(groupUsers[i].connected)
        {
            connectedUsers += "\n- " + groupUsers[i].nickname;
            continue;
        }
        disconnectedUsers += "\n- " + groupUsers[i].nickname;
    }

    outputMessage += "\n\nConnected members:";
    outputMessage += connectedUsers;
    if(disconnectedUsers !== "")
    {
        outputMessage += "\n\nDisconnected members:";
        outputMessage += disconnectedUsers;
    }
    data.text = outputMessage;
    return data;
}

/**
 *
 * Prepares the template options for getAllMembers command. If there is more than one option, calls executeCommandOnGroup with ExecuteGetAllMembers
 * @param data - The current data to send.
 * @param userMediaID - The sender's OSNS ID.
 * @returns {Promise<JSON>} - The modified data to send to the user.
 */
async function VerifyAndExecuteGetAllMembers(data, userMediaID)
{
    const userDatabase = await databaseManager.findUserByMediaID(userMediaID);
    const groups = await databaseManager.findGroupsByUserID(userMediaID);

    const templateOptions = commandHelper.getGroupIDsWithCondition(userDatabase.connectedGroups, groups,  true);

    if(templateOptions.length === 0)
    {
        data.text = "You are not connected to any groups yet. please use the !connecttogroup command first.";
        return data;
    }

    return await commandHelper.executeCommandOnGroup(data.message, ExecuteGetAllMembers,
        {
            "type":"talkToGroup",
            "command":"AL",
            "options":templateOptions
        }, data, userDatabase, userMediaID);
}

/**
 *
 * Disconnects the member that has userMediaID from the group with targetGroupID.
 * This means they will no longer receive messages from groups or be able to interact with the group until they reconnect to it.
 * @param data - The current data to send.
 * @param targetGroupID - The database ID of the group to disconnect the user from.
 * @param userMediaID - The user's OSNS media ID to disconnect from the group.
 * @returns {Promise<JSON>} - The modified data to send to the user.
 */
async function ExecuteDisconnectFromGroup(data, targetGroupID, userMediaID)
{
    const targetGroup = await databaseManager.findGroupByGroupID(targetGroupID);
    const userDatabase = await databaseManager.findUserByMediaID(userMediaID);

    await databaseManager.modifyConnectedUserToGroupByGroupID(targetGroup._id, userDatabase._id, false);
    await databaseManager.removeConnectGroupOfUserBYMediaID(userMediaID, targetGroupID);

    data.text = "Disconnected from group " + targetGroup.groupName + ".\nYou will no longer receive messages from this group."
    return data;
}

/**
 *
 * Prepares the template options for disconnectFromGroup command. If there is more than one option, calls executeCommandOnGroup with ExecuteDisconnectFromGroup
 * @param data - The current data to send.
 * @param userMediaID - The user's OSNS media ID.
 * @returns {Promise<JSON>} - The modified data to send to the user.
 */
async function VerifyAndExecuteDisconnectFromGroup(data, userMediaID)
{
    const userDatabase = await databaseManager.findUserByMediaID(userMediaID);
    const groups = await databaseManager.findGroupsByUserID(userMediaID);

    const templateOptions = commandHelper.getGroupIDsWithCondition(userDatabase.connectedGroups, groups,  true);

    if(templateOptions.length === 0)
    {
        data.text = "You are not connected to any groups.";
        return data;
    }
    if(userDatabase.state === "sendingToGroup")
    {
        data.text = "Cannot disconnect from group while a session is opened. Please type !quitSession first.";
        return data;
    }
    else if(userDatabase.state === "sendingToUser")
    {
        data.text = "Cannot disconnect from group while a session is opened. Please type !quitSession to return to the session to the group, then type !quitSession to quit the session to the group."
        return data;
    }

    return await commandHelper.executeCommandOnGroup(data.message, ExecuteDisconnectFromGroup,{
        "type":"talkToGroup",
        "command":"DI",
        "options":templateOptions
    }, data, userDatabase, userMediaID);
}

/**
 *
 * Executed when the user sends a message which is not a command.
 * Based on the user's state, the behavior changes:
 * . SendingToGroup: defines the target group to send to and sends the original message of the user.
 * . SendingToUser: defines the target group and receiver user to send to and sends the original message of the user.
 * . undefined: The user's message isn't a command, we prompt them to use the !help command to learn about commands.
 * @param data - The current data to send.
 * @param userMediaID - The user's OSNS media ID.
 * @returns {Promise<JSON>} - The modified data to send to the user.
 */
async function ExecuteNotACommand(data, userMediaID)
{
    const userDatabase = await databaseManager.findUserByMediaID(userMediaID);
    const userState = userDatabase.state;

    if(userState === "sendingToGroup")
    {
        data.text = data.message;
        data.targetGroup = userDatabase.target;
    }
    else if(userState === "sendingToUser")
    {
        data.text = data.message;
        data.userTarget = userDatabase.userTarget;
        data.targetGroup = userDatabase.target;
    }
    else
        data.text = "Could not recognize command.\nType !help for information on available commands.";

    return data;
}

/**
 *
 * Quits the session the user has opened.
 * Based on the user's state, the behavior changes:
 * . SendingToGroup: Quits the session opened with a group.
 * . SendingToUser: Quits the session opened with a group. The user then returns to the session with the group.
 * . undefined: The user doens't currently have a session opened. The user is prompted with this.
 * @param data - The current data to send.
 * @param userMediaID - The user's OSNS media ID.
 * @returns {Promise<JSON>} - The modified data to send to the user.
 */
async function ExecuteQuitSession(data, userMediaID)
{
    const userState = await databaseManager.findStateByMediaID(userMediaID);

    if(userState === "sendingToGroup")
    {
        await databaseManager.changeUserStateByMediaID(userMediaID, "");

        const targetGroup = await databaseManager.findGroupTargetByMediaID(userMediaID);

        const groupName = await databaseManager.findGroupNameByGroupID(targetGroup);
        data.text = "session with group " + groupName + " was closed.";
    }
    else if(userState === "sendingToUser")
    {
	    const userDatabase = await databaseManager.findUserByMediaID(userMediaID);

        await databaseManager.changeUserStateByMediaID(userMediaID, "sendingToGroup");
        await databaseManager.changeMemberGroupTargetByMediaID(userMediaID, "");
        const targetUser = await databaseManager.findUserByUserID(userDatabase.userTarget);
        const group = await databaseManager.findGroupByGroupID(userDatabase.target);
        const userNickname = databaseManager.findUserNicknameByUserIDInGroup(group, targetUser._id.toString());
        data.text = "Private session with " + userNickname + " closed. Messages will now be sent to everyone in the group " + group.groupName + ".\ntype \"!quitSession\" to close the " +
            "session and use other commands.";
    }
    else
    {
        data.text = "You are currently not connected to a group.";
    }

    return data;
}

/**
 *
 * Lists all the groups the user is connected to.
 * @param data - The current data to send.
 * @param userMediaID - The user's OSNS media ID.
 * @returns {Promise<JSON>} - The modified data to send to the user.
 * @constructor
 */
async function ExecuteGetConnectedGroups(data, userMediaID)
{
    const userDatabase = await databaseManager.findUserByMediaID(userMediaID);

    let text = "";
    userDatabase.connectedGroups.forEach(connectedGroup => {
        const foundGroup = userDatabase.groups.find(group => group.groupID.toString() === connectedGroup.toString())
        console.log("found : ", foundGroup);
        if(foundGroup !== undefined)
            text += "\n  - " + foundGroup.groupName;
    });
    if(text === "")
        data.text = "You are not connected to any groups.";
    else
        data.text = "You are connected to the groups:" + text;

    return data;
}

/**
 *
 * Opens a session with the target group in parameter.
 * @param data - The current data to send.
 * @param targetGroupID - The database ID of the group to open a session with.
 * @param userMediaID - The sender's OSNS media ID.
 * @returns {Promise<JSON>} - The modified data to send to the user.
 */
async function ExecuteOpenSessionToGroup(data, targetGroupID, userMediaID)
{
    const targetGroup = await databaseManager.findGroupByGroupID(targetGroupID);

    await databaseManager.changeUserStateByMediaID(userMediaID, "sendingToGroup")
    await databaseManager.changeGroupTargetByMediaID(userMediaID, targetGroup._id);
    data.text = "Opened session to group " + targetGroup.groupName + ".\ntype \"!quitSession\" to close the " +
        "session and use other commands.";
    return data;
}

/**
 * Opens a session with the target member in parameter in the scope of the target group. The target group is defined by the target group stored for the user in the database.
 * @param data - The current data to send.
 * @param targetMemberID - The database ID of the member to open a session with.
 * @param userMediaID - The sender's OSNS media ID.
 * @returns {Promise<JSON>} - The modified data to send to the user.
 */
async function ExecuteOpenSessionToMember(data, targetMemberID, userMediaID)
{
    const userDatabase = await databaseManager.findUserByMediaID(userMediaID);

    const targetGroup = await databaseManager.findGroupByGroupID(userDatabase.target);
    const targetMemberNickNameInGroup = await databaseManager.findUserNicknameByUserIDInGroup(targetGroup, targetMemberID);

    await databaseManager.changeUserStateByID(userDatabase._id, "sendingToUser");
    await databaseManager.changeMemberGroupTargetByMediaID(userMediaID, targetMemberID);

    data.text = "Opened session to user " + targetMemberNickNameInGroup + ".\ntype \"quitSession\" to close the " +
        "session to the user and talk to everyone in the group.";
    return data;
}

/**
 *
 * Prepares the template options for Opensession command in the scope of a group. If there is more than one option, calls executeCommandOnGroup with ExecuteOpenSessionToMember
 * @param data - The current data to send.
 * @param userDatabase - The user's database data.
 * @param userMediaID - The user's OSNS media ID.
 * @returns {Promise<JSON>} - The modified data to send to the user.
 */
async function VerifyAndExecuteOpenSessionToMember(data, userDatabase, userMediaID)
{
    const targetGroup = await databaseManager.findGroupByGroupID(userDatabase.target);

    if(!targetGroup)
    {
        console.error("could not find target group to opensession to member with.");
        return;
    }

    const groupUsers = targetGroup.users;

    let templateOptions = commandHelper.getGroupMemberNickNamesAndIDsWithCondition(groupUsers, true);

    //removes the sender from the possible options
    for(let index = 0; index < templateOptions.length; ++index)
    {
        if(templateOptions[index].optionValue.toString() === userDatabase._id.toString())
        {
            templateOptions.splice(index, 1);
        }
    }

    if(templateOptions.length === 0)
    {
        data.text = "No other user is currently connected to the group " + targetGroup.groupName + ".";
        return data;
    }

    return await commandHelper.executeCommandOnGroupMember(data.message, ExecuteOpenSessionToMember,
        {
        "type":"talkToGroup",
        "command":"DS",
        "options":templateOptions
    }, data, targetGroup, userDatabase._id, userMediaID);
}

/**
 *
 * Prepares the template options for Opensession command in the scope of a group. If there is more than one option, calls executeCommandOnGroup with ExecuteOpenSessionToMember
 * @param data - The current data to send.
 * @param userDatabase - The user's database data.
 * @param userMediaID - The user's OSNS media ID.
 * @returns {Promise<JSON>}
 */
async function VerifyAndExecuteOpenSessionToGroup(data, userDatabase, userMediaID)
{
    const groups = await databaseManager.findGroupsByUserID(userMediaID);

    const templateOptions = commandHelper.getGroupIDsWithCondition(userDatabase.connectedGroups, groups,  true);

    if(templateOptions.length === 0)
    {
        data.text = "You are currently not connected to a group. Please connect to a group with the command !connectToGroup first.";
        return data;
    }

    return await commandHelper.executeCommandOnGroup(data.message, ExecuteOpenSessionToGroup, {
        "type":"talkToGroup",
        "command":"SE",
        "options":templateOptions
    }, data, userDatabase, userMediaID);
}

/**
 * Opens a session to a group or a user.
 * Based on the user's state, the behavior changes:
 * . SendingToGroup: Opens a session with the selected or targeted member in the scope of the group.
 * . SendingToUser: Prompts the user that they can't open a session when already in a session with a user.
 * . undefined: Opens a session with the selected or targeted group the user is connected to.
 * @param data - The current data to send.
 * @param userMediaID - The sender's OSNS media ID.
 * @returns {Promise<JSON>} - The modified data to send to the user.
 */
async function ExecuteOpenSession(data, userMediaID)
{
    const userDatabase = await databaseManager.findUserByMediaID(userMediaID);

    if(userDatabase.state === "sendingToUser")
    {
        data.text = "Session with a user is already opened. type !quitSession first to open a session with someone else.";
        return data;
    }
    //If user is in a session to a group, open a session to a user
    if(userDatabase.state === "sendingToGroup")
        return await VerifyAndExecuteOpenSessionToMember(data,userDatabase, userMediaID);

    return await VerifyAndExecuteOpenSessionToGroup(data, userDatabase, userMediaID);

}

/**
 *
 * Prepares the data JSON to send the message to the group the user has an opened session with.
 * @param data - The current data to send.
 * @param userMediaID - The sender's OSNS media ID.
 * @returns {Promise<void>}
 */
async function ExecuteSendingToGroupMessage(data, userMediaID)
{
    const userDatabase = await databaseManager.findUserByMediaID(userMediaID);

    data.text = data.message;
    data.targetGroup = userDatabase.target;
}

/**
 *
 * Prepares the data JSON to send the message to the user the sender has an opened session with.
 * @param data - The current data to send.
 * @param userMediaID - The sender's OSNS media ID.
 * @returns {Promise<void>}
 */
async function ExecuteSendingToUser(data, userMediaID)
{
    const userDatabase = await databaseManager.findUserByMediaID(userMediaID);

    data.text = data.message;
    data.userTarget = userDatabase.userTarget;
    data.targetGroup = userDatabase.target;
}

/**
 * Connects the user to the target group in parameter.
 * @param data - The current data to send.
 * @param targetGroupID - The database ID of the target group.
 * @param userMediaID - The sender's OSNS media ID.
 * @returns {Promise<JSON>}
 */
async function ExecuteConnectToGroup(data, targetGroupID, userMediaID)
{
    const targetGroup = await databaseManager.findGroupByGroupID(targetGroupID);
    const userDatabase = await databaseManager.findUserByMediaID(userMediaID);

    await databaseManager.addConnectedGroupToUserByMediaID(userMediaID, targetGroupID.toString());
    await databaseManager.modifyConnectedUserToGroupByGroupID(targetGroupID, userDatabase._id, true);

    data.text = "\nConnected to group " + targetGroup.groupName + ".";

    if(targetGroup.messages !== undefined)
    {
        for(let i = targetGroup.users.find(user => user.userID.toString() === userDatabase._id.toString()).readMessageIndex; i < targetGroup.messages.length; ++i)
        {
            data.text += "\n" + targetGroup.messages[i];
        }
    }
    return data;
}

/**
 *
 * Prepares the template options for ConnectToGroup command. If there is more than one option, calls executeCommandOnGroup with ExecuteOpenSessionToMember
 * @param data - The current data to send.
 * @param userMediaID - The sender's OSNS media ID.
 * @returns {Promise<JSON>}
 */
async function VerifyAndExecuteConnectToGroup(data, userMediaID)
{
    const userDatabase = await databaseManager.findUserByMediaID(userMediaID);
    const groups = await databaseManager.findGroupsByUserID(userMediaID);

    const templateOptions = commandHelper.getGroupIDsWithCondition(userDatabase.connectedGroups, groups,  false);

    if(templateOptions.length === 0)
    {
        data.text = "You are either already connected to all the groups or you have not joined any groups yet."
        return data;
    }
    return await commandHelper.executeCommandOnGroup(data.message, ExecuteConnectToGroup,
        {
            "type":"talkToGroup",
            "command":"CO",
            "options":templateOptions
        }, data, userDatabase, userMediaID)
}

/**
 *
 * Prepares the data JSON with a formatted message listing the connected members in the target group.
 * @param data - The current data to send.
 * @param targetGroupID - The database ID of the target group.
 * @returns {Promise<JSON>}
 */
async function ExecuteGetConnectedMembers(data, targetGroupID)
{
    const targetGroup = await databaseManager.findGroupByGroupID(targetGroupID);

    const connectedUsers = await databaseManager.findConnectedMembersByGroupID(targetGroupID);
    data.text = "There are currently " + connectedUsers.length + " users currently connected to the group " + targetGroup.groupName + ". Those users are: ";

    for(let i = 0; i < connectedUsers.length; ++i)
    {
        data.text += "\n" +  connectedUsers[i].nickname;
    }
    return data;
}

/**
 *
 * Prepares the template options for GetConnectedMembers. If there is more than one option, calls executeCommandOnGroup with ExecuteGetConnectedMembers.
 * @param data - The current data to send.
 * @param userMediaID - The user's OSNS media ID.
 * @returns {Promise<JSON>}
 */
async function VerifyAndExecuteGetConnectedMembers(data, userMediaID)
{
    const userDatabase = await databaseManager.findUserByMediaID(userMediaID);
    const groups = await databaseManager.findGroupsByUserID(userMediaID);

    const templateOptions = commandHelper.getGroupIDsWithCondition(userDatabase.connectedGroups, groups,  true);

    if(templateOptions.length === 0)
    {
        data.text = "You are not connected to any groups yet. please use the !connecttogroup command first";
        return data;
    }
    return await commandHelper.executeCommandOnGroup(data.message,ExecuteGetConnectedMembers,
        {
            "type":"talkToGroup",
            "command":"CM",
            "options":templateOptions
        }, data, userDatabase, userMediaID);
}

async function ExecuteQuitGroup(data, targetGroupID, userMediaID)
{
    const targetGroup = await databaseManager.findGroupByGroupID(targetGroupID);
    const connectedUsers = await databaseManager.findConnectedMembersByGroupID(targetGroup._id);
    const userDatabase = await databaseManager.findUserByMediaID(userMediaID);


    connectedUsers.forEach(user => async function(){
        const userData = await databaseManager.findUserByUserID(user.userID);
        if(userData.userTarget.toString() === userDatabase._id.toString())
        {
            await databaseManager.changeUserStateByID(userData._id, "sendingToGroup");
            await databaseManager.changeMemberGroupTargetByUserID(userData._id, "");
        }
    })
    data.text = "Successfully quit group " + targetGroup.groupName + ".";
    data.message = data.text;
    await postHandler.postToPlatform(data);
    const userNickname = await databaseManager.findUserNicknameByUserIDInGroup(targetGroup, userDatabase._id);
    data.text = userNickname + " has left the group " + targetGroup.groupName + ".";
    data.message = data.text;
    data.targetGroup = targetGroupID;
    data.isQuitMessage = true;

    const userGroup = userDatabase.groups.find(group => group.groupID.toString() === targetGroupID.toString());
    const groupUser = targetGroup.users.find(user => user.userID.toString() === userDatabase._id.toString());

    await databaseManager.removeConnectGroupOfUserBYMediaID(userMediaID, targetGroupID.toString());
    await databaseManager.removeUserOfGroupByGroupID(targetGroupID, groupUser);
    await databaseManager.removeGroupOfUserByMediaID(userMediaID, userGroup);
    data.senderNickname = userNickname;

    return data;
}
async function VerifyAndExecuteQuitGroup(data, userMediaID)
{
    const userDatabase = await databaseManager.findUserByMediaID(userMediaID);
    const groups = await databaseManager.findGroupsByUserID(userMediaID);

    const templateOptions = commandHelper.getGroupIDsWithCondition(userDatabase.connectedGroups, groups,  true);

    if(templateOptions.length === 0)
    {
        data.text = "You are not connected to any groups yet.";
        return data;
    }
    if(userDatabase.state === "sendingToGroup")
    {
        data.text = "Cannot quit a group while a session is opened. Please type !quitSession first.";
        return data;
    }
    else if(userDatabase.state === "sendingToUser")
    {
        data.text = "Cannot quit a group while a session is opened. Please type !quitSession to return to the session to the group, then type !quitSession to quit the session to the group."
        return data;
    }
    return await commandHelper.executeCommandOnGroup(data.message, ExecuteQuitGroup,
        {
            "type":"talkToGroup",
            "command":"QT",
            "options":templateOptions
        }, data, userDatabase, userMediaID);

}
module.exports = {getAllMembers: ExecuteGetAllMembers, ExecuteOpenSession, VerifyAndExecuteGetAllMembers, ExecuteNotACommand, ExecuteQuitSession, ExecuteGetConnectedGroups, ExecuteSendingToGroupMessage, ExecuteSendingToUser, VerifyAndExecuteDisconnectFromGroup, VerifyAndExecuteConnectToGroup, VerifyAndExecuteGetConnectedMembers, VerifyAndExecuteQuitGroup, ExecuteOpenSessionToMember, ExecuteOpenSessionToGroup, ExecuteDisconnectFromGroup, ExecuteConnectToGroup, ExecuteGetConnectedMembers, ExecuteQuitGroup}
