/**
 * @module dataTranslator
 * @category Core
 * @description Receives formatted JSONs from servers. based on the command sent to the user, redirects to the command executor which applies the command.
 */
const mqt = require('./rabbitMQ/send.js')
const commandExecutor = require('./commandExecutor')

/**
 *
 * Interprets the serverData JSON. Based on the command used, calls the correct command executor and gets the message to send to the user.
 * The reply data is then sent to the dedicated rabbitMQ queue of the server it originates from.
 * @param {JSON} serverData - The JSON translated into Socialbus' JSON format.
 * @param {Boolean} isUser - Differentiates if the message is from a user or not.
 */
async function sendMessage(serverData, isUser)
{

	let data = {};
	data.connectors = serverData.connectors;
	data.origin = serverData.origin;
	data.message = serverData.text;

	let userText = serverData.text.toLowerCase();

	const userMediaID = serverData.connectors.id;

	let isCommand = false;
	if(userText[0] === "!")
	{
		isCommand = true;
		userText = userText.substring(1);
	}
	if(!isUser)
	{
		data.text = "Could not recognize command.\nType help for information on available commands.";
	}
	else if(!isCommand)
	{
		data = await commandExecutor.ExecuteNotACommand(data, userMediaID);
	}
	else if(userText === "quitsession")
	{
		data = await commandExecutor.ExecuteQuitSession(data, userMediaID);
	}
	else if(userText.includes("getallmembers"))
	{
		data = await commandExecutor.VerifyAndExecuteGetAllMembers(data, userMediaID);
	}
	else if(userText === "getconnectedgroups")
	{
		data = await commandExecutor.ExecuteGetConnectedGroups(data, userMediaID);
	}
	else if(userText.includes("opensession"))
	{
		data = await commandExecutor.ExecuteOpenSession(data, userMediaID);
	}
	else if(userText.includes("disconnectfromgroup"))
	{
		data = await commandExecutor.VerifyAndExecuteDisconnectFromGroup(data, userMediaID);
	}
	else if(userText.includes("connecttogroup"))
	{
		data = await commandExecutor.VerifyAndExecuteConnectToGroup(data, userMediaID);
	}
	else if(userText.includes("getconnectedmembers"))
	{
		data = await commandExecutor.VerifyAndExecuteGetConnectedMembers(data, userMediaID);
	}
	else if(userText.includes("quitgroup"))
	{
		data = await commandExecutor.VerifyAndExecuteQuitGroup(data, userMediaID);
	}
	else if(userText === "help")
	{
		data.text = '  Available commands:  \n'+
			'    \n'+
			'    - connectToGroup [joined group name]: receive the latest messages in a group and receive new messages in real time.  \n'+
			'    - connectToGroup: opens a prompt to choose a group to connect to.  \n'+
			'    \n'+
			'    - disconnectFromGroup [connected group name]: stop receiving messages of a group in real time.  \n'+
			'    - disconnectFromGroup : opens a prompt to choose which group to disconnect.  \n'+
			'    \n'+
			'    \n'+
			'    - openSession [connected group name]: opens a session to a connected group. every message sent will be automatically sent to the group until exited.  \n'+
			'    - openSession : opens a prompt to choose which connected group to open a session with.  \n'+
			'    \n'+
			'    - quitSession: disconnect from active session if there is one.  \n'+
			'    \n'+
			'    - getConnectedGroups : lists currently connected groups  \n'+
			'    \n'+
			'    - getConnectedMembers [joined group name]: lists currently connected members  \n'+
			'    - getConnectedMembers: opens a prompt to choose a group to list the connected members from.  \n'+
			'    \n'+
			'    - getAllMembers [joined group name]: lists all members that joined the group. \n'+
			'    - getAllMembers: opens a prompt to choose a group to list the members from.  \n'+
			'      ';
	}
	else
	{
		data.text = "Could not recognize command.\nType \"!help\" for information on available commands.";
	}

	let dataString = JSON.stringify(data);
	mqt.sendMessage(dataString, data.origin);
}

/**
 *
 * Interprets the serverData JSON. Based on the type of button pressed, calls the command executor and gets the text to send to the user.
 * The reply data is then sent to the dedicated rabbitMQ queue of the server it originates from.
 * To know how to interpret the data, the first three first characters of the buttonID stored in serverData define which command it is.
 * SE_: open session
 * DI_: disconnect from group
 * DS_: open session with a specific user in a group
 * CO_: connect to group
 * CM_: get connected members
 * Al_: get all members
 * QT_: quit group
 * @param {JSON} serverData - The JSON translated into Socialbus' JSON format.
 * @returns {Promise<void>}
 */
async function handleButtonDatabase(serverData)
{
	let data = {};
	data.connectors = serverData.connectors;
	data.origin = serverData.origin;
	console.log(serverData.buttonID);

	const userMediaID = serverData.connectors.id;

	const buttonValue = serverData.buttonID.slice(3,serverData.buttonID.length);

	if(serverData.buttonID.includes("SE_"))
	{
		data = await commandExecutor.ExecuteOpenSessionToGroup(data, buttonValue, userMediaID);
	}
	else if (serverData.buttonID.includes("DI_"))
	{
		data = await commandExecutor.ExecuteDisconnectFromGroup(data, buttonValue, userMediaID);
	}
	else if(serverData.buttonID.includes("DS_"))
	{
		data = await commandExecutor.ExecuteOpenSessionToMember(data, buttonValue, userMediaID);
	}
	else if(serverData.buttonID.includes("CO_"))
	{
		data = await commandExecutor.ExecuteConnectToGroup(data, buttonValue, userMediaID);
	}
	else if(serverData.buttonID.includes("CM_"))
	{
		data = await commandExecutor.ExecuteGetConnectedMembers(data, buttonValue);
	}
	else if(serverData.buttonID.includes("AL_"))
	{
		data = await commandExecutor.VerifyAndExecuteGetAllMembers(data, buttonValue);
	}
	else if(serverData.buttonID.includes("QT_"))
	{
		data = await commandExecutor.ExecuteQuitGroup(data, buttonValue, userMediaID);
	}
	else
		data.text = "Button error";
	let dataString = JSON.stringify(data);
	mqt.sendMessage(dataString,data.origin);
}

module.exports = { sendMessage, handleButtonDatabase};
