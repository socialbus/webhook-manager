/**
 * @module postToService
 * @category Core
 * @description postToService sends finalised messages to OSNS'.
 */
const request = require('request');
const requestPromise = require('request-promise');
const databaseManager = require('./mongoose.js');

const mqt = require('./rabbitMQ/send.js');
const template = require('./buildTemplate.js');

require('dotenv').config({path:"../.env"})

/**
 *
 * Redirects to the group or OSNS to send the message to.
 * If the targetGroup key is defined, themessage is sent to a whole group.
 * Otherwise, the reply is meant for the OSNS the user's message originates from.
 * @param {JSON} messageJson - The finalized message to send.
 */
async function postToPlatform(messageJson)
{
    let targetQueue = messageJson.origin;

    console.log("targetQueue is equal to : " + targetQueue);
    if(messageJson.userTarget !== undefined)
    {
        const targetUser = await databaseManager.findUserByUserID(messageJson.userTarget);
        const sender = await databaseManager.findUserByMediaID(messageJson.connectors.user);
        const group = await databaseManager.findGroupByGroupID(messageJson.targetGroup);
        const userNickname = databaseManager.findUserNicknameByUserIDInGroup(group, sender._id.toString());
        const message = "Direct message from " + userNickname + "@" + group.groupName + ": " + messageJson.message;
        postToGroupMember(targetUser, messageJson, message);
        return;
    }
    if(messageJson.targetGroup !== undefined)
    {
        console.log("target group");
        postToGroup(messageJson);
        return
    }
    if(targetQueue === "Slack")
    {
        postToSlack(messageJson);
    }
    else if(targetQueue === "Messenger")
    {
        postToMessenger(messageJson);
    }
    else if(targetQueue === "Twitter")
    {
        postToTwitter(messageJson)
    }
    else if(targetQueue === "Website")
    {
        postToSite(messageJson);
    }
    else if(messageJson.target !== undefined)
    {
        messageJson.origin = messageJson.target;
        messageJson.target = undefined;
        messageJson.text = messageJson.message;

        mqt.sendMessage(JSON.stringify(messageJson), messageJson.origin);
    }
    else
    {
        console.log("SERVICE NOT FOUND")
    }

}

/**
 *
 * Sends the message to a member in all of social medias they have connected with.
 * @param {JSON} user - The user to send the message to.
 * @param {JSON} messageJson - The finalized data to send.
 * @param {string} message - The formatted message to send.
 */
function postToGroupMember(user, messageJson, message)
{
    for(let j = 0; j < user.medias.length; ++j) {
        let jsonToSend = {};
        if (user.medias[j].media === "Twitter") {
            jsonToSend = {
                "connectors": {
                    "bot_id": messageJson.connectors.bot_id,
                    "user": user.medias[j].id
                },
                "origin": "Twitter",
                "text": message
            };
        } else if (user.medias[j].media === "Messenger") {
            jsonToSend = {
                "connectors": {
                    "sender": {
                        "id": user.medias[j].id
                    }
                },
                "origin": "Messenger",
                "text": message
            };
        } else if (user.medias[j].media === "Slack") {

            jsonToSend = {
                connectors: {
                    channel: user.medias[j].channel
                },
                "origin": "Slack",
                "text": message
            }

        }
        console.log("sending json: ", jsonToSend);
        postToPlatform(jsonToSend);
    }
}

/**
 *
 * Sends the message to everyone connected user in the targeted group.
 * @param {JSON} messageJson - The finalized message to send.
 */
async function postToGroup(messageJson)
{
    console.log("messageJson: " , messageJson);
    const sender = await databaseManager.findUserByMediaID(messageJson.connectors.user.toString());
    console.log("userID:", messageJson.connectors.user);
    console.log("sender: ", sender);
    const group = await databaseManager.findGroupByGroupID(messageJson.targetGroup);
    const users = await databaseManager.findUsersInGroup(group);

    let userNickname = "";
    if(messageJson.senderNickname)
        userNickname = messageJson.senderNickname;
    else
        userNickname = databaseManager.findUserNicknameByUserIDInGroup(group, sender._id.toString());
    
    let message = messageJson.message;
    if(!messageJson.isJoinMessage && !messageJson.isQuitMessage)
         message = userNickname + "@" + group.groupName + ": " + messageJson.message;
    await databaseManager.storeMessageInGroup(messageJson.targetGroup, message);

    let receivedUserIDs = [];
    for(let i = 0; i < users.length; i++)
    {
        if(users[i]._id.toString() === sender._id.toString()) continue;
        if(!users[i].connectedGroups.includes(messageJson.targetGroup)) continue;
        //If the user is connected, the read message index is updated to the current message
        receivedUserIDs.push(users[i]._id.toString());
        console.log("preparing for user: ", users[i]);
        postToGroupMember(users[i], messageJson, message);
    }

    await databaseManager.updateReadMessageIndexUsersByUserIDs(group._id, receivedUserIDs, group.messages.length);
}

/**
 *
 * Sends the finalized message to the user in Slack.
 * If a button template is defined in the "buttonTemplate" key of the messageJson parameter, sends a button template instead of a message.
 * @param {JSON} messageJson - The finalized message to send.
 */
async function postToSlack(messageJson)
{
    let data;

    data = {
        form: {
            "token":process.env.SLACK_BOT_TOKEN,
            "channel": messageJson.connectors.channel
        }
    };
    messageJson.buttonTemplate !== undefined?
        data.form.blocks = JSON.stringify(await template.buildButtonTemplate(messageJson.buttonTemplate, messageJson.origin, messageJson.connectors.user)):
        data.form.text = messageJson.text;

    request.post('https://slack.com/api/chat.postMessage', data, function (err, res, body) {
        if (!err && res.statusCode === 200) {
            console.log("message sent to Slack!\n");
            console.log("body is :" + body)
        } else {
            console.log("there was an error");
        }
    });
}

function postToTwitter(messageJson)
{
    let nbOptions = 1; //TODO determine amount of options
    if(nbOptions > 3)
    {
        for(let i = 0; i < (nbOptions/3); i++)
        {
            sendMessageToTwitter(messageJson);
            messageJson.text = "options";
        }
    }
    else
    {
        sendMessageToTwitter(messageJson);
    }

}

/**
 *
 * Sends the finalized message to the user in Twitter.
 * If a button template is defined in the "buttonTemplate" key of the messageJson parameter, sends a button template instead of a message.
 * @param {JSON} messageJson - The finalized message to send.
 */
async function sendMessageToTwitter(messageJson)
{

    let data;
    data = {
        "type": "message_create",
        "message_create": {
            "target": {
                recipient_id: messageJson.connectors.user
            },
            "message_data": {
                text: messageJson.text,
                //"quick_reply": quickReply,

            }
        }
    }
    if(messageJson.buttonTemplate !== undefined)
    {
        console.log("template, origin, user:", messageJson.buttonTemplate, messageJson.origin, messageJson.connectors.user);
        data.message_create.message_data = await template.buildButtonTemplate(messageJson.buttonTemplate, messageJson.origin, messageJson.connectors.user);
    }

    let twitter_oauth = {
        consumer_key: process.env.TWITTER_CONSUMER_KEY,
        consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
        token: process.env.TWITTER_TOKEN,
        token_secret: process.env.TWITTER_TOKEN_SECRET,
    };

    let direct_message_options =
        {
            url: 'https://api.twitter.com/1.1/direct_messages/events/new.json',
            oauth: twitter_oauth,
            json:true,
            headers:{
                'content-type':'application/json',
            },
            body:{
                event:data
            },
        };

    console.log("event data is :", data);
    requestPromise.post(direct_message_options).then(function(err, res, body) {
        console.log("message sent to twitter");
    }).catch(function(err){
        console.log("error : ", err);
    });

}

/**
 *
 * Sends the finalized message to the user in Messenger.
 * If a button template is defined in the "buttonTemplate" key of the messageJson parameter, sends a button template instead of a message.
 * @param {JSON} messageJson - The finalized message to send.
 */
async function postToMessenger(messageJson)
{
    let data;
    data = {
        recipient:{
            id:messageJson.connectors.sender.id
        },
        message:{
        },
    };
    if(messageJson.buttonTemplate !== undefined)
    {
        data.message.attachment = await template.buildButtonTemplate(messageJson.buttonTemplate, messageJson.origin, messageJson.connectors.sender.id)
    }
    else
    {
        data.message.text = messageJson.text;
    }
    console.log("data is : ", data);
    request({
        "uri": "https://graph.facebook.com/v2.6/me/messages",
        "qs": { "access_token": process.env.MESSENGER_BOT_TOKEN },
        "method": "POST",
        "json": data
    }, (err, res, body) => {
        if (!err)
        {
            console.log("body:" , body);
            console.log('message sent to Messenger!');
        }
        else {
            console.error("Unable to send message:" + err);
        }
    });
}

function postToSite(messageJson)
{
    const data = {
        "userID": messageJson.userID,
        "databaseUserID": messageJson.databaseUserID,
        "media": messageJson.media
    }
    request({
        "uri": "http://express:" + process.env.EXPRESSPORT + "/updateMedia",
        "method": "POST",
        "json": data
    }, (err, res, body) => {
        if (!err)
        {
            console.log("user media updated");
        }
        else {
            console.error("Unable to update user:" + err);
        }
    });

}


module.exports = {postToPlatform, postToGroupMember};
