/**
 * @module mongoose
 * @category Database
 * @description Handles connection to the mongoDB database and queries to it.
 */

const mongoose = require('mongoose');

mongoose.connect('mongodb://' + (process.env.MONGO_HOST || "127.0.0.1") + ':' + (process.env.MONGO_PORT || 27017) + '/website',
    {useNewUrlParser : true})

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));

function onOpen()
{
    console.log("connected to database");
}
db.once('open', () => onOpen());

let groupSchema = new mongoose.Schema({
    key: { type :String, required : true, unique : true},
    groupName: String,
    users: [
        {
            userID:mongoose.Schema.Types.ObjectId,
            nickname: String,
            connected:Boolean,
            readMessageIndex:{ type: Number, default: 0}
        }
        ],
    messages : Array
});

let userSchema = new mongoose.Schema({
    medias: [
        {
            "media": String,
            "id": String,
	        "channel": String
        }
    ],
    groups:[{
        groupID:mongoose.Schema.Types.ObjectId,
        groupName: String,
        nickname:String
    }],
    connectedGroups : Array,
    state: String,
    target: String,
    userTarget: String
}, {timestamps: { createdAt: 'created_at'}});

const user = mongoose.model('user', userSchema);
const group = mongoose.model('group', groupSchema);

/**
 *
 * Finds the groups of the user with the media ID in parameter.
 * @param {string} mediaID - A user's OSNS id.
 * @returns {PromiseLike<*>} The user's groups.
 */
const findGroupsByMediaID = (mediaID) => user.findOne({"medias.id":mediaID}).exec().then(user => user.groups);

/**
 *
 * Finds the connected groups of the user with the media ID in parameter.
 * @param {string} mediaID - A user's OSNS id.
 * @returns {PromiseLike<*>} The user's connected groups.
 */
const findConnectedGroupsByMediaID = (mediaID) => user.findOne({"medias.id":mediaID}).exec().then(user => user.connectedGroups);

/**
 *
 * Finds the group with the group ID in parameter.
 * @param {string} groupID - A group ID.
 * @returns {PromiseLike<*>} The found group.
 */
const findGroupByGroupID = (groupID) => group.findOne({"_id":groupID}).exec();

/**
 *
 * Finds the group name of the group with the ID in parameter.
 * @param {string} groupID - A group ID.
 * @returns {PromiseLike<*>} The found group's name.
 */
const findGroupNameByGroupID = (groupID) => group.findOne({"_id":groupID}).exec().then(group => group.groupName);

/**
 *
 * Finds the groups with the group IDs in parameter.
 * @param {Array<string>} groupIDs - Multiple group IDs.
 * @returns {PromiseLike<*>} The found groups.
 */
const findGroupsbyGroupIDs = (groupIDs) => group.find({"_id":{$in : groupIDs}}).exec();

/**
 *
 * Finds the users with the user IDs in parameter.
 * @param {Array<string>} usersIDs - Multiple user IDs.
 * @returns {PromiseLike<*>} The found users.
 */
const findUsersbyUsersIDs = (usersIDs) => user.find({"_id":{$in : usersIDs}}).exec();


const updateReadMessageIndexUsersByUserIDs = (groupID, userIDs, readMessageIndex)=> group.findOne({"_id":groupID},  {returnOriginal:false}).exec().then(async function(foundGroup){
    let includedUsers = foundGroup.users.filter(user => userIDs.includes(user.userID.toString()));

    includedUsers.forEach(user => user.readMessageIndex = readMessageIndex);

    await foundGroup.save();

});
/**
 * const modifyConnectedUserToGroupByGroupID = (groupID, userID, connectTo) => group.findOne({"_id":groupID},  {returnOriginal:false}).exec().then(async function(foundGroup){
 *     let groupUser = foundGroup.users.find(groupUser => groupUser.userID.toString() === userID.toString());
 *     groupUser.connected = connectTo;
 *
 *     await foundGroup.save();
 * });
 */

/**
 *
 * Finds a user with the user ID in parameter.
 * @param {string} userID - A user's OSNS id.
 * @returns {PromiseLike<*>} The found user.
 */
const findUserByUserID = (userID) => user.findOne({"_id":{$in : userID}}).exec();

/**
 *
 * Finds the groups of the user with the media ID in parameter.
 * @param {string} mediaID - A user's OSNS id.
 * @returns {PromiseLike<*>} The found user
 */
const findUserByMediaID = (mediaID) => user.findOne({"medias.id":mediaID}).exec();

/**
 *
 * Changes the user's state found with the ID in parameter.
 * @param {string} userID - A user's id.
 * @param {string} state - The user state to set.
 */
const changeUserStateByID = (userID, state) => user.findOneAndUpdate({"_id":{$in : userID}}, {"state":state}, {returnOriginal:false}).exec();

/**
 *
 * Changes the user's state found with the media ID in parameter.
 * @param {string} mediaID - A user's OSNS id.
 * @param {string} state - The user state to set.
 */
const changeUserStateByMediaID = (mediaID, state) => user.findOneAndUpdate({"medias.id": mediaID}, {"state":state}, {returnOriginal:false}).exec();


/**
 *
 * Finds the user's state with the media ID in parameter.
 * @param {string} mediaID - A user's OSNS id.
 * @returns {PromiseLike<*>} The found user's state.
 */
const findStateByMediaID = (mediaID) => user.findOne({"medias.id":mediaID}).exec().then(user => user.state);

/**
 *
 * Changes the target group for a user found with the media ID in parameter.
 * @param {string} mediaID - A user's OSNS id.
 * @param {string} target - The group to target
 */
const changeGroupTargetByMediaID = (mediaID, target) => user.findOneAndUpdate({"medias.id": mediaID}, {"target":target}, {returnOriginal:false}).exec();

/**
 * Changes the target member the user with the media ID in parameter is sending a direct message to.
 * @param {string} mediaID - A User's OSNS id
 * @param {string} target - A User's id to send the messages to.
 */
const changeMemberGroupTargetByMediaID = (mediaID, target) => user.findOneAndUpdate({"medias.id": mediaID}, {"userTarget":target}, {returnOriginal:false}).exec();


/**
 * Changes the target member the user with the user database ID in parameter is sending a direct message to.
 * @param {string} userID - A User's database id
 * @param {string} target - the group to send the messages to
 */
const changeMemberGroupTargetByUserID = (userID, target) => user.findOneAndUpdate({"_id": {$in: userID}}, {"userTarget":target}, {returnOriginal:false}).exec();


/**
 *
 * Finds the targeted group of the user with the media ID in parameter.
 * @param {string} mediaID - A user's OSNS id.
 * @returns {PromiseLike<*>} The found user's targeted group.
 */
const findGroupTargetByMediaID = (mediaID) => user.findOne({"medias.id":mediaID}).exec().then(user => user.target);

/**
 *
 * Adds a group in the connected groups of a user found with the media ID in the parameter.
 * @param {string} mediaID - A user's OSNS id.
 * @param {string} groupID - the group ID to add.
 */
const addConnectedGroupToUserByMediaID = (mediaID, groupID) => user.findOneAndUpdate({"medias.id":mediaID}, {$addToSet:{"connectedGroups": groupID}}, {returnOriginal:false}).exec();

/**
 *
 * Sets the connected flag of a user in the group to true or false.
 *
 * @param {string} groupID - The group ID to search the user in.
 * @param {string} userID - The user ID to search in the found group.
 * @param {boolean} connectTo - the value to set to the connected flag of the user in the group.
 */
const modifyConnectedUserToGroupByGroupID = (groupID, userID, connectTo) => group.findOne({"_id":groupID},  {returnOriginal:false}).exec().then(async function(foundGroup){
    let groupUser = foundGroup.users.find(groupUser => groupUser.userID.toString() === userID.toString());
    groupUser.connected = connectTo;

    await foundGroup.save();
});

/**
 *
 * Finds all the connected users in the searched group.
 *
 * @param {string} groupID - The group ID to search the connected users in.
 * @returns {Promise<Array<string>>} The connected users of the group
 */
const findConnectedMembersByGroupID = (groupID) => group.findOne({"_id":groupID}).exec().then(foundGroup => {
    let connectedUsers = [];
    const users = foundGroup.users;
    for(let i = 0; i < users.length; ++i)
    {
        if(users[i].connected)
            connectedUsers.push(users[i]);
    }
    return connectedUsers;
});

/**
 * create a blank user with the predfined mongoose Schema values.
 * @returns {Promise<*>} - the created user
 */
async function createBlankUser()
{
    const newUser = new user;
    await newUser.save();
    return newUser;
}

/**
 * Deletes the user with the ID in parameter from the database.
 * @param {string}userID- the user to delete
 */
const deleteUserByUserID = (userID) => user.findOneAndDelete({"_id":userID}).exec();
/**
 *
 * Remove a group from the connected groups of a user found with the media ID in the parameter.
 * @param {string} mediaID - A user's OSNS id.
 * @param {string} groupID - the group ID to remove.
 */
const removeConnectGroupOfUserBYMediaID = (mediaID, groupID) => user.findOneAndUpdate({"medias.id":mediaID}, {$pull:{"connectedGroups": groupID}}, {returnOriginal:false}).exec();

/**
 *
 * Remove a group from a user found with the media ID in the parameter.
 * @param {string} mediaID - A user's OSNS id.
 * @param {JSON} group - the group to remove.
 */
const removeGroupOfUserByMediaID = (mediaID, group) => user.findOneAndUpdate({"medias.id":mediaID}, {$pull:{"groups": group}}, {returnOriginal:false}).exec();

/**
 *
 * Remove a user from a group found with the group ID in the parameter.
 * @param {string} groupID - A group's database ID.
 * @param {JSON} user - the user to remove.
 */
const removeUserOfGroupByGroupID = (groupID, user) => group.findOneAndUpdate({"_id":groupID}, {$pull:{"users": user}}, {returnOriginal:false}).exec();


/**
 *
 * Finds a group's latest messages found with the group ID in parameter.
 * @param {string} groupID - A group ID.
 * @returns {Array<string>} The latest messages in the found group.
 */
const findMessagesByGroupID = (groupID) => group.findOne({"_id":groupID}).exec().then(group => group.messages);

/**
 *
 * Changes the group's messages with the ones in parameter found with the group ID in parameter.
 * @param {string} groupID - A group ID.
 * @param {Array<string>} messages - The messages to store in the found group.
 */
const changeGroupMessagesByGroupID = (groupID, messages) => group.findOneAndUpdate({"_id":{$in : groupID}}, {"messages":messages}, {returnOriginal:false}).exec();

/**
 *
 * Remove the user's media in parameter found by the media ID in parameter.
 * @param {string} mediaID - A user's OSNS id.
 * @param {{"media": String, "id": String, "channel": String}} media - The media to remove.
 */
const removeMediaOfUserBYMediaID = (mediaID, media) => user.findOneAndUpdate({"medias.id":mediaID}, {$pull:{"medias":{ media: media}}}, {returnOriginal:false}).exec();


/**
 *
 * Replaces the media in parameter to the user's media found by the media ID in parameter.
 * @param {string} mediaID - A user's OSNS id.
 * @param {{"media": String, "id": String, "channel": String}} media - The media to replace.
 */
const updateMediaWithMediaId = (mediaID, media) => user.findOneAndUpdate({"medias.id":mediaID},{$addToSet:{medias: media}}, {returnOriginal:false}).exec();

const changeMediaChannelByMediaID = (mediaID, channel) => user.update({"medias.id": mediaID}, {"$set": { "medias.$.channel":channel}});

/**
 *
 * Formats a string with the current time.
 * This is used to be stored in the messages of a group.
 * @returns {string} string of the current time formatted.
 */
function getTimeInMessage()
{
    //TODO store the user's time zone and change the displayed time based on it.
    const date_ob = new Date();
    const day = ("0" + date_ob.getDate()).slice(-2);
    const month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
    const year = date_ob.getFullYear();
    const hours = date_ob.getHours();
    const minutes = date_ob.getMinutes();

    return year + "-" + month + "-" + day + " | " + hours + ":" + minutes
}

/**
 *
 * Stores a new message in the group with the id in parameter.
 * If the maximum amount of messages is passed, removes the oldest message to add the new one.
 * The time is also added to the message.
 * @param {string} groupID - The group ID to store the message in.
 * @param {string} message - The message to store.
 */
async function storeMessageInGroup(groupID, message)
{
    if(message === undefined || !message)
    {
        console.log("no message provided");
        return;
    }
    const group = await findGroupByGroupID(groupID);
    const groupMessages = group.messages;// await findMessagesByGroupID(groupID);
    const timeStr = getTimeInMessage();

    const messageWithDate = timeStr + " " + message;
    groupMessages.push(messageWithDate);
    if(groupMessages.length.toString() === process.env.MESSAGE_AMOUNT_LENGTH){
        groupMessages.shift();

        group.users.forEach(user => {
            if(user.connected || user.readMessageIndex === 0)
                return;
            --user.readMessageIndex;
        })
    }
    await group.save();
    //await changeGroupMessagesByGroupID(groupID, groupMessages);
}

/**
 *
 * Finds all the groups of the user found with the media ID in parameter.
 * @param mediaID - The user's media ID
 * @returns {Promise<*>} The found groups.
 */
async function findGroupsByUserID(mediaID)
{
    const groupsOfUser = await findGroupsByMediaID(mediaID);

    const groupsIDs = groupsOfUser.map(group => group.groupID);
    console.log("groupsID", groupsIDs);

    return await findGroupsbyGroupIDs(groupsIDs);
}

/**
 *
 * Finds the user's nickname found with the user ID in parameter in the group in parameter.
 * @param {JSON} group - The group containing the user.
 * @param {string} userID - The user's ID.
 * @returns {StringConstructor | String} the user's nickname in the group.
 */
function findUserNicknameByUserIDInGroup(group, userID)
{
    console.log("group users found:", group.users);
    console.log("user to find is:", userID);

    const user = group.users.find(user => user.userID.toString() === userID.toString());
    if(user === undefined)
    {
        console.error("could not find user in group");
        return undefined;
    }
    console.log("found user: ", user);
    return user.nickname;
}


/**
 *
 * Finds all the users in the group in parameter.
 * @param {JSON} group -
 * @returns {Promise<*>}
 */
async function findUsersInGroup(group)
{
    const usersIDs = group.users.map(users => users.userID);
    console.log("usersID", usersIDs);

    return await findUsersbyUsersIDs(usersIDs);
}


/**
 *
 * Replaces an existing media with a new json value
 * @param {{id : string|number, media: string}} media - values to change. The mediaID must be the same as the existing one.
 * @property id - id of the social media
 * */
async function replaceUserMedia(media)
{
    let userToUpdate = await findUserByMediaID(media.id);

    if(userToUpdate === null)
        console.error("could not update user media: Invalid UserID");

    for(let i = 0; i < userToUpdate.medias.length; ++i)
    {
        if(userToUpdate.medias[i].media.toString() !== media.media)
            continue;
        await userToUpdate.medias.splice(i, 1, media);
        break;
    }
    await userToUpdate.save();
}

module.exports = {deleteUserByUserID, createBlankUser, findConnectedMembersByGroupID, changeMemberGroupTargetByUserID, modifyConnectedUserToGroupByGroupID, replaceUserMedia, removeMediaOfUserBYMediaID, removeGroupOfUserByMediaID, removeUserOfGroupByGroupID, updateMediaWithMediaId, storeMessageInGroup, removeConnectGroupOfUserBYMediaID, addConnectedGroupToUserByMediaID, findUserByMediaID, findUserNicknameByUserIDInGroup,
    findGroupNameByGroupID, changeGroupTargetByMediaID, changeMemberGroupTargetByMediaID, findGroupTargetByMediaID, findStateByMediaID, changeUserStateByMediaID,
    changeUserStateByID, findUsersInGroup, findGroupsByMediaID, findConnectedGroupsByMediaID, updateReadMessageIndexUsersByUserIDs, findGroupsbyGroupIDs,
    findGroupByGroupID, findUsersbyUsersIDs, findGroupsByUserID, findUserByUserID};
