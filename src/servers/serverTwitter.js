/**
 * @module serverTwitter
 * @category Servers
 * @description Requests sent by Twitter are received here.
 */
'use strict';
const express = require('express');
const parser = require('body-parser');
const app = express().use(parser.json());
app.use(parser.urlencoded({extended: true}));

const messageHandler = require('../dataTranslator.js');
const crypto = require('crypto');
let isUser;
require('dotenv').config({path:"../.env"})

function onListen()
{
    console.log("Twitter webhook is listening on port ", process.env.TWITTERPORT);
}

/**
 *
 * Receive messages from Twitter users and translates the JSON into Socialbus' JSON format.
 * The JSON is then sent to dataTranslator.js
 * @param {JSON} req - Received HTTP request
 * @param {JSON} res - Sending HTTP request
 */
function onPost(req, res)
{
    let challenge = req.body.challenge;
    res.json({
        challenge:challenge,
    });

    console.log("body in json : ", req.body);

    let body = req.body;

    //This means it is not a direct message nor directed to bot
    if(body.for_user_id === undefined || /*body.for_user_id !== botUserID ||*/ !body.direct_message_events) {
        console.log("not directed to bot");
        return;
    }

    let directMessage = body.direct_message_events[0];
    let messageCreate = directMessage.message_create;

    let quickReplyResponse = messageCreate.message_data.quick_reply_response;
    console.log("quick reply res is : ", quickReplyResponse);


    if(quickReplyResponse !== undefined)
    {
        let response = {
            connectors:{
                bot_id: process.env.TWITTER_BOT_ID, //maybe can get this automatically?
                user: messageCreate.sender_id, //first index is sender
                id:messageCreate.sender_id
            },
            text: messageCreate.message_data.text,
            buttonID:quickReplyResponse.metadata,
            origin:"Twitter",
        };

        console.log("the response is", JSON.stringify(response));
        messageHandler.handleButtonDatabase(response);
        return;
    }
    let response = {
        connectors:{
          bot_id:  process.env.TWITTER_BOT_ID,
            user: messageCreate.sender_id, //first index is sender
            id:messageCreate.sender_id

        },
        text: messageCreate.message_data.text,
        origin:"Twitter"

    };

    console.log("response is :", response);
    if(messageCreate.sender_id !==  process.env.TWITTER_BOT_ID && response.connectors.bot_id !== "BRMFB9YNM")
    {
        console.log("is a user!");
       isUser = true;
       messageHandler.sendMessage(response,isUser);
    }
    else
    {
        console.log("isn't a user!");

        isUser = false;
    }

}

/**
 *
 * Receive button presses from Twitter users and translates the JSON into Socialbus' JSON format.
 * The JSON is then sent to dataTranslator.js
 * @param {JSON} req - Received HTTP request
 * @param {JSON} res - Sending HTTP request
 */
function onPostButton(req,res)
{
    console.log("button pressed");
    let payload = req.body.payload;
    let payloadJSON = payload;//let payloadJSON = JSON.parse(payload); !!!!!!! might be mandatory?
    let actions = payloadJSON.actions;
    let action_id;
    actions[0].selected_option !== undefined?
        action_id = actions[0].selected_option.value:
        action_id = actions[0].action_id;
    console.log("actions : " + JSON.stringify(actions));

    console.log("action id is : " + JSON.stringify(action_id))
    let response = {
        connectors: {
            bot_id: payloadJSON.message.bot_id,
            channel: payloadJSON.channel.id,
            user: payloadJSON.user.id,
            //user:payload.user?
            token: payloadJSON.token,
            id: payloadJSON.user.id,

        },
        buttonID:action_id,
        origin: "Twitter"
    };

    console.log("the response is", JSON.stringify(response));


    messageHandler.handleButtonDatabase(response);


}

/**
 *
 * Receive get requests from Twitter. This is called by Twitter to validate the application.
 * If the token is valid, send response 200, otherwise 400 to indicate an error.
 * @param {JSON} req - Received HTTP request
 * @param {JSON} res - Sending HTTP request
 */
function onGet(req, res)
{

    console.log("query is: " , req.query);
    console.log("params is: ", req.params);
    console.log("crc token: ", req.query.crc_token);

    let crc_token = req.query.crc_token;

    //If it is hashed properly with the consumer_secret, the token is valid.
    if (crc_token) {
        let hash = crypto.createHmac('sha256', process.env.TWITTER_CONSUMER_SECRET).update(crc_token).digest('base64')

        res.status(200);
        res.send({
            response_token: 'sha256=' + hash
        })
        return hash;

    } else {
        res.status(400);
        res.send('Error: crc_token missing from request.')
    }

}
function onGetButton(req,res)
{
    res.sendStatus(200);
}
app.listen(process.env.TWITTERPORT || 5000, onListen());
app.post('/webhook', (req,res) => onPost(req, res));
app.post('/button', (req,res) => onPostButton(req,res));
app.get('/webhook',(req,res) => onGet(req,res));
app.get('/button/', (req,res) => onGetButton(req,res));

module.exports = app;
