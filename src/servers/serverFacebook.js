/**
 * @module serverFacebook
 * @category Servers
 * @description Requests sent by Facebook are received here.
 */
'use strict';

const express = require('express');
const parser = require('body-parser');
const app = express().use(parser.json());
const messageHandler = require('../dataTranslator.js');
const postHandler = require('../postToService');
const databaseManager = require('../mongoose')
require('dotenv').config({path:"../.env"})


function onListen()
{
    console.log("Messenger webhook is listening on port ", process.env.MESSENGERPORT);
}

/**
 *
 * Receive messages from Messenger users and translates the JSON into Socialbus' JSON format.
 * The JSON is then sent to dataTranslator.js.
 * If the message is received with a referral field, it means that the request comes from a user joining a group.
 * In this case, the message is sent to the backend of the website with the user's Messenger ID.
 * @param {JSON} req - Received HTTP request
 * @param {JSON} res - Sending HTTP request
 */
async function onPost(req, res)
{
    let body = req.body;
    console.log("body:", body);

    if(body.isJoinMessage && !body.targetGroup)
    {
        const user = await databaseManager.findUserByUserID(body.connectors.id);
        if(!user)
            return;
        postHandler.postToGroupMember(user, body, body.message)
        return;
    }
    if(body.isJoinMessage)
    {
        postHandler.postToPlatform(body);
        return;
    }

    if (body.object === 'page')
    {
        body.entry.forEach(async function(entry)
        {
            let body = entry.messaging[0];
            console.log("body is :" + JSON.stringify(body));

            let sender_psid = body.sender.id;
            console.log("Sender PSID: ", sender_psid);
            let isUser;
            let response;

            //If this is the case, send the user's ID to the website's backend.
            if(body.optin !== undefined)
            {
                response = {
                  connectors: {
                      sender:
                          {
                              id: body.sender.id
                          },
                      recipient:
                          {
                              id: body.recipient.id
                          },
                      user: body.sender.id,
                      id: body.sender.id
                  },
                  userID : body.sender.id,
                  databaseUserID: body.optin.ref,
                  origin: "Website",
                  media:"Messenger",
                  text: "Thank you for using Socialbus. Please finish creating or joining the group on the website."

                };
                console.log("verify user validity");
                const databaseUser = await databaseManager.findUserByUserID(response.databaseUserID);
                if(!databaseUser)
                    return;
                else if(databaseUser.medias.find(media => media.media === "Messenger"))
                    return;
                await postHandler.postToPlatform(response);
                response.origin = "Messenger"
                postHandler.postToPlatform(response);
                return;
            }
            else if(body.referral !== undefined && body.referral.type === "OPEN_THREAD")
            {
                response = {
                  connectors: {
                        sender:
                            {
                                id: body.sender.id
                            },
                        recipient:
                            {
                                id: body.recipient.id
                            },
                        user: body.sender.id,
                        id: body.sender.id
                    },
                    userID: body.sender.id,
                    databaseUserID: body.referral.ref,
                    origin: "Website",
                    media:"Messenger",
		            text: "Thank you for using Socialbus. Please finish creating or joining the group on the website."
                };
                console.log("verify user validity");
                const databaseUser = await databaseManager.findUserByUserID(response.databaseUserID);
                if(!databaseUser)
                    return;
                else if(databaseUser.medias.find(media => media.media === "Messenger"))
                    return;
                await postHandler.postToPlatform(response);
                response.origin = "Messenger"
                postHandler.postToPlatform(response);

                return;
            }
            response = {
                connectors: {
                    sender:
                        {
                            id: body.sender.id
                        },
                    recipient:
                        {
                            id: body.recipient.id
                        },
                    user: body.sender.id,
                    id: body.sender.id
                },
                origin: "Messenger",
            };


            console.log("messenger bot ID:", process.env.MESSENGER_BOT_ID);
            sender_psid !== process.env.MESSENGER_BOT_ID?
                isUser = true:
                isUser = false;
            console.log("this is a user:  " + isUser);

            //If this message is from a button press from a user, send to handleButtonDatabase instead.
            if(body.message && isUser)
            {
                response.text = body.message.text;
                messageHandler.sendMessage(response, isUser);
            }
            else if (body.postback && isUser)
            {
                response.buttonID = body.postback.payload;
                messageHandler.handleButtonDatabase(response);
            }

        });
        res.status(200).send('RECEIVED EVENT');
    }
    else
    {
        res.sendStatus(404);
    }

}

/**
 *
 * Receive get requests from Messenger. This is called by Messenger to validate the application.
 * If the token is valid, send response 200, otherwise 400 to indicate an error.
 * @param {JSON} req - Received HTTP request
 * @param {JSON} res - Sending HTTP request
 */
function onGet(req, res)
{
    let mode = req.query['hub.mode'];
    let token = req.query['hub.verify_token'];
    let challenge = req.query['hub.challenge'];
    console.log("token : " + token);
    console.log("challenge : " + challenge);
    if(mode && token) // if those exist
    {
        if(mode === 'subscribe' && token === process.env.MESSENGER_VERIFY_TOKEN)
        {
            console.log('WEBHOOK VERIFIED');
            res.status(200).send(challenge);
        }
        else
        {
            console.log('invalid challenge.')
            //did not receive token
            res.sendStatus(400);
        }
    }
    else
    {

        console.log("token or mode undefined");
        res.sendStatus(400);
    }
}

app.listen(process.env.MESSENGERPORT || 5001, onListen());
app.post('/webhook', (req,res) => onPost(req, res));
app.get('/webhook',(req,res) => onGet(req,res));

module.exports = app;
