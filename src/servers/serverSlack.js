/**
 * @module serverSlack
 * @category Servers
 * @description Requests sent by Slack are d here.
 */
'use strict';
const express = require('express');
const parser = require('body-parser');
const app = express().use(parser.json());
app.use(parser.urlencoded({extended: true}));
const messageHandler = require('../dataTranslator.js');
const databaseManager = require("../mongoose");

require('dotenv').config({path:"../.env"})
let isUser;

const { expressCspHeader, INLINE, NONE, SELF } = require('express-csp-header');

app.use(expressCspHeader({
	directives: {
		'default-src': [SELF],
		'img-src': [SELF],
	}
}));

function onListen()
{
	console.log("Slack webhook is listening on port", process.env.SLACKPORT);
}

/**
 *
 * Receive messages from Slack users and translates the JSON into Socialbus' JSON format.
 * The JSON is then sent to dataTranslator.js
 * @param {JSON} req - Received HTTP request
 * @param {JSON} res - Sending HTTP request
 */
async function onPost(req, res)
{
	let challenge = req.body.challenge;
	res.json({
		challenge:challenge,
	});

	let body = req.body.event;
	let response;

	console.log("event:", JSON.stringify(req.body.event));


	response = {
		connectors:{
			bot_id: body.bot_id,
			channel: body.channel,
			user: body.user,
			id: body.user

		},
		text: body.text,
		origin:"Slack"

	};

	if(response.connectors.bot_id === undefined && response.connectors.bot_id !== "BRMFB9YNM") // get this verification automatically?
	{
		isUser = true;
		const userDb = await databaseManager.findUserByMediaID(body.user);
		//Store the channel the user is sending the message from.
		if(userDb !== undefined)
		{
			let slack = userDb.medias.find(media => media.media.toString() === "Slack")
			slack.channel = body.channel;
			await databaseManager.replaceUserMedia(slack);
		}

		messageHandler.sendMessage(response,isUser);

	}
	else
	{
		isUser = false;
	}

}

/**
 *
 * Receive button presses from Slack users and translates the JSON into Socialbus' JSON format.
 * The JSON is then sent to dataTranslator.js
 * @param {JSON} req - Received HTTP request
 * @param {JSON} res - Sending HTTP request
 */
function onPostButton(req,res)
{

	let payload = req.body.payload;
	let payloadJSON = JSON.parse(payload);
	let actions = payloadJSON.actions;
	let action_id;
	actions[0].selected_option !== undefined?
		action_id = actions[0].selected_option.value:
		action_id = actions[0].action_id;
	console.log("actions : " + JSON.stringify(actions));

	console.log("action id is : " + JSON.stringify(action_id))
	let response = {
		connectors: {
			bot_id: payloadJSON.message.bot_id,
			channel: payloadJSON.channel.id,
			user: payloadJSON.user.id,
			//user:payload.user?
			token: payloadJSON.token,
			id: payloadJSON.user.id

		},
		buttonID:action_id,
		origin: "Slack"
	};

	console.log("the response is", JSON.stringify(response));


	messageHandler.handleButtonDatabase(response);


}

/**
 *
 * Receive get requests from Slack. This is called by Slack to validate the application.
 * If the token is valid, send response 200, otherwise 403 to indicate an error.
 * @param {JSON} req - Received HTTP request
 * @param {JSON} res - Sending HTTP request
 */
function onGet(req, res)
{
	console.log("get request");
	let mode = req.query['hub.mode'];
	let token = req.query['hub.verify_token'];

	if(mode && token) // if those exist
	{
		if(mode === 'subscribe' && token === process.env.SLACK_VERIFY_TOKEN)
		{
			console.log('WEBHOOK VERIFIED');
			res.status(200).send("received get request");
		}
		else
		{
			//did not receive token
			res.sendStatus(403);
		}
	}
}
function onGetButton(req,res)
{
	console.log("get from button!");
}
app.listen(process.env.SLACKPORT || 5002, onListen());
app.post('/webhook', (req,res) => onPost(req, res));
app.post('/button', (req,res) => onPostButton(req,res));
app.get('/webhook',(req,res) => onGet(req,res));
app.get('/button', (req,res) => onGetButton(res,res));

