/**
 * @module commandHelper
 * @category Core
 * */

/**
 *
 * Verifies if the user's command targets a specific group.
 * If only one group is possible, automatically apply the action to that member.
 * Otherwhise, verifies if the user sent a valid group name. If it is valid, we apply the action to that group.
 * If it isn't or the user hasn't sent any group name, prompt the button template in parameter to the user.
 * @param {string} command - The user's command.
 * @param {function} action - Function to apply if the command already targets a valid group.
 * @param {{type : string, command: string, options:JSON}} buttonTemplate - The button template to send to the user if we cannot determine who to apply the action to.
 * @param {JSON} data - The initial data that will be modified
 * @param {JSON} userDatabase - The user's data stored in the database.
 * @param {string} userMediaID - The user's media ID on the OSNS the message is received from.
 * @returns {Promise<JSON>} The newly formed data.
 */
async function executeCommandOnGroup(command, action, buttonTemplate, data, userDatabase, userMediaID)
{
    let words = command.split(" ");
    words.shift();

    let changedData = data;
    const userGroups = userDatabase.groups;

    if(userGroups.length === 0){
        changedData.text = "You have joined any groups. Please join or create a group on the website.;"
        return changedData;
    }
    else if(userGroups.length === 1)
        return action(data, userGroups[0].groupID.toString(), userMediaID);

    if(words.length === 0)
        changedData.buttonTemplate = buttonTemplate;

    let targetGroup = undefined
    for(let i = 0; i < userGroups.length; ++i)
    {
        const groupNameArray = userGroups[i].groupName.split(" ");
        if(groupNameArray.every(name => words.includes(name)))
        {
            targetGroup = userGroups[i].groupID.toString();
            break;
        }
    }
    if(targetGroup !== undefined)
    {
        changedData = action(data, targetGroup, userMediaID);
    }
    else
        changedData.buttonTemplate = buttonTemplate;

    return changedData;
}

/**
 *
 * Verifies if the user's command targets a specific member.
 * If only one member is possible, automatically apply the action to that member.
 * Otherwhise, verifies if a valid group member is available. if the user is valid, apply the action in parameter with the specified group member.
 * If it isn't, prompt the user with the button template in parameter to select which user the command is applied to.
 * @param {string} command - The user's command
 * @param {function} action - Function to apply if the command already targets a valid group member.
 * @param {{type : string, command: string, options:JSON}} buttonTemplate - The button template to send to the user if we cannot determine who to apply the action to.
 * @param {JSON} data - The initial data that will be modified
 * @param {JSON} targetGroup  - Member interactions are in the scope of a group. this is the group in which the members are.
 * @param {string} senderID - The database ID of the sender of the command.
 * @param {string} userMediaID - The user's OSNS media ID.
 * @returns {Promise<JSON>} The newly formed data.
 */
async function executeCommandOnGroupMember(command, action, buttonTemplate, data, targetGroup, senderID, userMediaID)
{
    let words = command.split(" ");
    words.shift();

    let changedData = data;

    let connectedUsers = [];
    const users = targetGroup.users;
    for(let i = 0; i < users.length; ++i)
    {
        if(users[i].connected && users[i].userID.toString() !== senderID.toString())
            connectedUsers.push(users[i]);
    }

    if(connectedUsers.length === 1)
        return action(data, connectedUsers[0].userID,  userMediaID);
    else if(words.length === 0)
        changedData.buttonTemplate = buttonTemplate;

    let targetUser = undefined;
    for(let i = 0; i < connectedUsers.length; ++i)
    {
        const memberNameArray = connectedUsers[i].nickname.split(" ");
        if(memberNameArray.every(name => words.includes(name)))
        {
            targetUser = connectedUsers[i].userID;
            break;
        }
    }
    if(targetUser !== undefined)
    {
        changedData = action(data, targetUser, userMediaID);
    } else
        changedData.buttonTemplate = buttonTemplate;

    return changedData;
}

/**
 *
 * Either retrieve all the group IDs the user is connected to or all the group IDs the user isn't connected to.
 * @param {{nickname: string, connected: boolean, userID: string}} groupUsers - All the users in the group to retrieve the nicknames from.
 * @param {Boolean} getConnectedMembers - Determines if connected or disconnected members are retrieved.
 * @returns {Array<string|number>} - All the connected or disconnected members within the group Users in the button template JSON format.
 */
function getGroupMemberNickNamesAndIDsWithCondition(groupUsers, getConnectedMembers)
{
    let usersData = [];
    for(let userIndex = 0; userIndex < groupUsers.length; ++userIndex)
    {
        const currentUser = groupUsers[userIndex];
        if(getConnectedMembers && currentUser.connected)
        {
            usersData.push({
                "optionValue":currentUser.userID,
                "optionName":currentUser.nickname
            });
        }
        else if(!getConnectedMembers && !currentUser.connected)
        {
            usersData.push({
                "optionValue":currentUser.userID,
                "optionName":currentUser.nickname
            });
        }
    }

    return usersData;
}

/**
 *
 * Either retrieve all the group IDs the user is connected to or all the group IDs the user isn't connected to.
 * @param {{id : string|number, media: string}} userConnectedGroups - Groups the user is connected to.
 * @param {{id : string|number, media: string}} groups - All the groups the user has joined.
 * @param {Boolean} getConnectedGroups - Determines what kind of groups to return.
 * @returns {Array<string|number>} - Groups returned.
 */
function getGroupIDsWithCondition(userConnectedGroups, groups, getConnectedGroups)
{
    let groupIDs = [];
    for(let i = 0; i < groups.length; i++)
    {
        if(getConnectedGroups && userConnectedGroups.includes(groups[i]._id))
            groupIDs.push({
                "optionValue":groups[i]._id,
                "optionName":groups[i].groupName
            });
        else if(!getConnectedGroups && !userConnectedGroups.includes(groups[i]._id))
            groupIDs.push({
                "optionValue":groups[i]._id,
                "optionName":groups[i].groupName
            });
    }
    return groupIDs;
}

/**
 *
 * Reformats groups data of a user to be usable for button templates as available options.
 * @param {JSON} groups - groups to reformat
 * @returns {Array<string|number>} - Button template formatted group options
 */
function getGroupIDsButtonTemplateFormat(groups)
{
    let groupIDs = [];
    for(let i = 0; i < groups.length; i++)
    {
        groupIDs.push({
                "optionValue":groups[i]._id,
                "optionName":groups[i].groupName
        });
    }
    return groupIDs;
}


module.exports = {getGroupMemberNickNamesAndIDsWithCondition, executeCommandOnGroup, executeCommandOnGroupMember, getGroupIDsWithCondition, getGroupIDsButtonTemplateFormat}
